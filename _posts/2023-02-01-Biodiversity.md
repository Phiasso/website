---
layout: post
title: "Biodiversity"
permalink: "/biodiversity/"
date:   2023-02-01 13:12:00 +0200
categories: ecology
tags: biodiversity
published: false
lang: en 
lang-ref: biodiv
---



Biodiversity:

- Consequences of changing biodiversity https://www.nature.com/articles/35012241
- Biodiversity at multiple trophic levels is needed for ecosystem multifunctionality https://www.nature.com/articles/nature19092

