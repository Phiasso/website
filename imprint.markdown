---
layout: page
title: Imprint
permalink: "/imprint/"
url: 'https://karpenko.fr/imprint'
---



Sophia Karpenko

Universitätsstr. 10, 78464 Konstanz



## Privacy Policy

Sophia Karpenko operates the website "kaso" at https://karpenko.fr. 

**Collection of Routine Information**

This website does not track its visitors. Whether it be IP addresses, browser details, timestamps and referring pages - none of it is collected. 



**Cookies**

This website does not use cookies.



**Advertisement and Other Third Parties**

This website does not have any ads, advertising partners or third party tracking. So no cookies, scripts or web beacons to track visitor activities. 



**Changes To This Privacy Policy**

This Privacy Policy is effective as of 2020-03-01 and will remain in effect except concerning any changes in its provisions in the future, which will be in effect immediately after being posted on this page. 



**Contact Information**

For any questions or concerns regarding the privacy policy, please send me an email at sophia@karpenko.fr.
