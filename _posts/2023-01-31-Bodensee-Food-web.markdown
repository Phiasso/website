---
layout: post
title: "Food web of lake Constance"
permalink: "/bodensee-food-web/"
date:   2023-01-31 15:12:00 +0200
categories: ecology
tags: bodensee lake constance food web
published: false
lang-ref: food web
lang: en
---

*Little ecological stories from lake Constance: Part 2*



Fishing tradition

Trophic status and link to fishes?

In the 60s: eutrophication and overfishing

Fishing practices:

- Purse seine ("Klusgarn"): https://www.fao.org/3/AA044E/AA044E06.htm
- today: https://en.wikipedia.org/wiki/Gillnetting size-selective
- size-selection reduces genetic diversity: https://onlinelibrary.wiley.com/doi/10.1111/jfb.12393 

Food-web modeling: https://onlinelibrary.wiley.com/doi/10.1111/j.1461-0248.2012.01777.x

Size-selectivity for fish eating mussels: https://pubmed.ncbi.nlm.nih.gov/35298838/

Human-induced changes on C. lavaretus:

-  https://onlinelibrary.wiley.com/doi/10.1111/j.1420-9101.2008.01622.x
- https://kops.uni-konstanz.de/bitstream/handle/123456789/8750/Thomas_Eckmann_CJFAS_2007.pdf?sequence=1&isAllowed=y

Felchen, Phosphor, Berufsfischerei: http://wiki.bodenseekonferenz.org/Attachments/Faktenblatt_Felchen%20und%20Phosphor_Brinker.pdf

Limnologischer zustand des bodensees: https://www.igkb.org/fileadmin/user_upload/dokumente/publikationen/gruene_berichte/44_gb44gesamtbericht.pdf



Kormorane:

-  https://de.wikipedia.org/wiki/Kormoran_(Art)#Bejagung
- https://nora.nerc.ac.uk/id/eprint/14330/1/N014330CR.pdf
- https://www.ibkf.org/wp-content/uploads/2018/03/IBKF_Kormoranstudie_Bodensee_2017.pdf
- https://www.kmae-journal.org/articles/kmae/ref/2014/03/kmae140012/kmae140012.html



**What about fish?**  

The great cormorant is a protected bird, which feeds on all available fish. As other fish-eating birds, the cormorant was suspected (very often wrongly!) to be competing for the fish ressources with humans and thus almost hunted to extinction in continental Europe by 1920. Through protective measures, their population has increased - also at lake Constance - and are considered stable today. The reputation of the cormorant among fishermen, however, has not improved: it is accused of being responsible of the decline of the whitefish population in lake Constance. Evidence shows, however, that the whitefish population declines with the increasing control of waste waters rejected into the lake; a measure to fight the pollution of the lake that happened in the second half of the 20th century.

Sticklebacks are non-native to the Bodensee and were most likely introduced by aquarium hobbyists or fishermen in the late 19th century. The introduced individuals formed a population and became abundant by the 1960’s. Today they represent about 28% of the fish biomass (or the second most abundant fish species) in the lake. Since they have no economical value, they are a nuisance for fisherman when they represent the majority of their catch. Nonetheless, they've become a non-negligeable food source for some birds, like the great cormorant. 

Quagga and Zebramussel are also two human-introduced species in the lake, most likely by boats. The Zebramussel was the first to arrive in the 1960s. It quickly colonized all the hard subtrate it could find (rocks, canalisations) and became a great source of food for overwintering birds, like the Tafelente, whose numbers rose considerably thanks to that. The Quagga mussel was first observed in 2016. It virtually replaced the Zebramussel within 6 years. Indeed, the quagga can also grow on fine and loose substrate (so be careful when you bathe !) and in deeper waters than the zebra. The overwintering birds and some fish, like the Rotauge, quickly adapted to this abundant new source of food.