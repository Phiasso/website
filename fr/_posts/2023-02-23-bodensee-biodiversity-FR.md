---
layout: post
title: "Histoires d'écologie du lac de Constance partie III "
permalink: "/fr/bodensee-biodiversity/"
date:   2023-02-21 10:57:00 +0200
categories: ecology
tags: bodensee lac constance biodiversité
lang-ref: food web
lang: fr
published: false
---

## La diversité c'est important

<font color="#00b07c"> Eh ouais.</font>  

### h3



###  Diversité Planctonique

https://esajournals.onlinelibrary.wiley.com/doi/abs/10.1890/06-1056.1

https://sci-hub.st/https://doi.org/10.1890/06-1056.1

> Since species richness does not adequately capture ecological function  and life history of different taxa, features which are important for  mechanistic theories, relationships between zooplankton functional  diversity (FD) and resource conditions were examined. Zooplankton  species richness showed the previously established tendency to a  unimodal relationship with TP, but functional diversity declined  linearly over the same gradient. Changes in zooplankton functional  diversity could be attributed to changes in both the spatial  distribution and type of phytoplankton resource. In the studied lakes,  spatial heterogeneity of phytoplankton groups declined with TP, even  while biomass of all groups increased.



**What about fish?**  

The great cormorant is a protected bird, which feeds on all available fish. As other fish-eating birds, the cormorant was suspected (very often wrongly!) to be competing for the fish ressources with humans and thus almost hunted to extinction in continental Europe by 1920. Through protective measures, their population has increased - also at lake Constance - and are considered stable today. The reputation of the cormorant among fishermen, however, has not improved: it is accused of being responsible of the decline of the whitefish population in lake Constance. Evidence shows, however, that the whitefish population declines with the increasing control of waste waters rejected into the lake; a measure to fight the pollution of the lake that happened in the second half of the 20th century.

Sticklebacks are non-native to the Bodensee and were most likely introduced by aquarium hobbyists or fishermen in the late 19th century. The introduced individuals formed a population and became abundant by the 1960’s. Today they represent about 28% of the fish biomass (or the second most abundant fish species) in the lake. Since they have no economical value, they are a nuisance for fisherman when they represent the majority of their catch. Nonetheless, they've become a non-negligeable food source for some birds, like the great cormorant. 

Quagga and Zebramussel are also two human-introduced species in the lake, most likely by boats. The Zebramussel was the first to arrive in the 1960s. It quickly colonized all the hard subtrate it could find (rocks, canalisations) and became a great source of food for overwintering birds, like the Tafelente, whose numbers rose considerably thanks to that. The Quagga mussel was first observed in 2016. It virtually replaced the Zebramussel within 6 years. Indeed, the quagga can also grow on fine and loose substrate (so be careful when you bathe !) and in deeper waters than the zebra. The overwintering birds and some fish, like the Rotauge, quickly adapted to this abundant new source of food.



**Felchen**:

- trophic state

  - Felchen, Phosphor, Berufsfischerei: http://wiki.bodenseekonferenz.org/Attachments/Faktenblatt_Felchen%20und%20Phosphor_Brinker.pdf

  - nice illustrations and felchen and phosphorus https://www.faz.net/aktuell/wissen/leben-gene/felchenschwund-dem-bodensee-gehen-die-begehrten-speisefische-aus-17225536.html

    

Poissons introduits: 

- Sandre, Regenbogenforelle
- Sonnenbarsch https://www.tagblatt.ch/ostschweiz/stgallen/fremder-fotzel-ohne-grossen-wert-ld.2082410

Poissons general:

- Lake Constance fisheries and fish ecology https://kops.uni-konstanz.de/server/api/core/bitstreams/ac78e094-da55-4547-810c-63701ecf3e09/content
- Long-term changes in littoral fish community structure and resilience of total catch to re-oligotrophication in a large, peri-alpine European lake : https://onlinelibrary.wiley.com/doi/full/10.1111/fwb.13501

Ecosystem (biological invasions) :

- Shifting trophic control of fishery–ecosystem dynamics following biological invasions https://esajournals.onlinelibrary.wiley.com/doi/full/10.1002/eap.2190?sid=nlm%3Apubmed
- food-web modeling: https://onlinelibrary.wiley.com/doi/10.1111/j.1461-0248.2012.01777.x
- Does eutrophication-driven evolution change aquatic ecosystems? https://royalsocietypublishing.org/doi/10.1098/rstb.2016.0041#d1e260





[^zehner]: Michael Zehner. [Die Ordnung der Fischer. Nachhaltingkeit und Fischerei am Bodensee (1350-1900)](http://dx.doi.org/10.7788/boehlau.9783412217549). 2014.
[^ autre ]: blabla





