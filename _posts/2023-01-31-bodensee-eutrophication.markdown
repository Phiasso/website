---
layout: post
title: " Little ecological stories from lake Constance part I "
permalink: "/bodensee-eutrophication/"
date:   2023-01-31 13:12:00 +0200
categories: ecology
tags: bodensee lake constance anthropogenic cultural eutrophication
lang-ref: beutroph
lang: en
---



## 1960s anthropogenic eutrophication of lake Constance

<font color="#00b07c"><b>Or the massive man-made pollution of one of the cleanest lakes of Europe</b> <br><small>(but apparently, <a href="https://en.wikipedia.org/wiki/Eutrophication#Extent_of_the_problem">that's normal</a>).</small></font>


### Lake what?

It's the far South of Germany, on the border with Switzerland and Austria that lies one of the biggest European lakes <small>(to settle on the question of who has the biggest one: "Lake Constance is the [third largest](https://en.wikipedia.org/wiki/List_of_largest_lakes_of_Europe) freshwater lake by surface area in Central and Western Europe (and the second largest in volume), after Lake Geneva and (in surface area) Lake Balaton", *source: [wikipedia](https://en.wikipedia.org/wiki/Lake_Constance#Description)*)</small>.

<div class="img-with-text" style="float:right">
<img src="/images/Einzugsgebietskarte_bodensee_723x1024.jpg" alt="Lake Constance catchment area" width="300" title="Lake Constance catchment area"/>
<p>
<font size="2"  color="#D67A00">Lake Constance catchment area (11 000 km²).</font>
</p>
</div>


It was carved out by the Rhine glacier during the last ice age ([Würm glaciation](https://en.wikipedia.org/wiki/W%C3%BCrm_glaciation)), which ended about 10 000 years ago, starting the Holocene geological epoch. This also corresponds (+/- several thousand years...) to the Neolithic period, which has left some vestiges on the lake's banks in the form of remains of [pile dwellings](https://en.wikipedia.org/wiki/Stilt_house). 


The name *Lake Constance* derived from *Lacus Constantinus* given during the Council of Constance (XV century), an alternative name given to the *Bodensee* ("Bodmansee" = the lake by Bodman, the latter being a small locality in the North-West of the upper lake, that used to have a larger influence in the Middle Ages).  

[Like in most of Europe](https://ourworldindata.org/grapher/population?time=1100..latest&country=DEU%7EEurope%7EEuropean+Union+%2827%29), the human lake population grew massively in the XIX and XX century ([Constance went from a stable over centuries 5000 inhabitants in 1800 to over 75 000 in 2000](https://de.wikipedia.org/wiki/de:konstanz?uselang=en#Bev%C3%B6lkerungsentwicklung )), the lake region underwent a heavy intensification of agricultural activity, urbanization and artificialization of its catchment. This was not without consequences for the lacustrine ecosystem. 



### "Cultural" eutrophication

In the second half of the XX century, the lake suffered a great episode of pollution, making it shift from an [*oligotrophic* state](https://en.wikipedia.org/wiki/Trophic_state_index#Oligotrophic) - characteristic of perialpine lakes, with a low biological productivity, resulting in clear waters - to a [*eutrophic* state](https://en.wikipedia.org/wiki/Trophic_state_index#Eutrophic), with an increased biological productivity, which manifested through a proliferation of phytoplankton, i.e. green and opaque waters. The [trophic state of a lake](https://en.wikipedia.org/wiki/Trophic_state_index#) is mainly determined by the nutrient availability, namely nitrogen and phosphorus, which tend to be limiting resources in standing water bodies. 



**Increase in nutrient availability causing algal proliferation.** In the case we're examining, the phosphorus concentrations in the Constance lake waters [were multiplied by 10 between the 1950s and the late 1970s](https://www.igkb.org/fileadmin/user_upload/dokumente/aktuelles/Faktenblaetter/2018-10-18_IGKB_Faktenblatt_Phosphor.pdf). This was due to the increase of the amount of waste waters that were dumped into the lake and the runoff of fertilizers, the use of which exploded after WWII - from agricultural activity in the catchment zone. Feces, detergents (back then) and fertilizers being pumped up with **phosphorus**. The situation became completely.. shitty in the 70s, as authorities had to restrict access to some beaches during high season, due to a high risk of spread of waterborne diseases: cholera and poliomyelitis.



**Ecological implications of eutrophication.** From an ecological perspective, one could think that if there's a lot of algae, that would mean more food for fish or mollusks... But it's more complicated. Rapid algal proliferation (algal blooms) on the water surface reduces light penetration into the deeper parts of the lake and can prevent other photosynthetic organisms (other algae!) from eating the sunlight. So they die, and eventually the algal bloom dies as well, and they all sink to the bottom of the lake, where they are decomposed by microorganisms. But microorganisms use up a lot of oxygen when they do that, and so the more they have to process, the less oxygen available there is at the bottom of the lake for other organisms that need it for respiration.. and so they die. And this creates hypoxic zones, to a larger scale [dead zones](https://en.wikipedia.org/wiki/Dead_zone_(ecology)), where *nothing can live*.

<div class="img-with-text" style="float:right">
	<a href="https://commons.wikimedia.org/w/index.php?curid=7502779">
  <img src="https://upload.wikimedia.org/wikipedia/commons/6/60/Kilch.jpg" alt="RIP" width="300" title="Bodensee-Kilch"/>
  </a>
  <p>
    <font size="2"  color="#D67A00">The late Kilch, Coregonus <i>gutturosus.</i> RIP.</font>
  </p>
</div>


At least one (emblematic) species suffered fatally from the hypoxic conditions at the lake bottom: Lake Constance whitefish, [Coregonus *gutturosus*](https://en.wikipedia.org/wiki/Coregonus_gutturosus) aka the Bodensee-Kilch. Its eggs could not develop with reduced amounts of oxygen, which led to the complete extinction of the species probably in the 70s. But C. *gutturosus* is still here, somewhere, with us, not only in our hearts, but also in the genetic material of other species of the Coregonus genus, like for example C. *macrophthalmus*, aka the Gangfish [^vonlanthen]. Indeed, the extinction of the Kilch did not solely happen through a demographic decline of the population, but also through a process called "introgressive hybridization" with closely related species. What happened is that C. *gutturosus* started to reproduce with C. *macrophtalmus*, which created hybrids of these two species. These hybrids would then reproduce with both species, but less with *gutturosus*, since its population was declining. Over generations, this led to an enrichment of *macrophtalmus*'s DNA with the DNA of *gutturosus* and a **speciation reversal** of *gutturosus* [^rhymer].

Loss of speciation (the process opposite to the appearance of a new species) is problematic in that it can participate in a general trend of (bio-)diversity loss through human-triggered ecosystem degradations [^rosenzweig]. And this trend is generally seen during the processes of eutrophication, not only for whitefish but also for plankton communities [^barnett] [^wang]. And biodiversity loss is bad. But it's not trivial so this I'll discuss another time.

**Lake turnover.** So oxygen is important not to become extinct. And in deep lakes, like lake Constance (250 m !), oxygen availability at the bottom is a major issue. And the way oxygen usually gets to the bottom (because since it's at the bottom, there's no light, so no photosynthesis, so no oxygen) is through [lake turnovers](https://en.wikipedia.org/wiki/Lake_ecosystem#Temperature). Deep lakes have a temperature stratification during warm seasons: the surface water temperature is that of the air (20 °C for example), while the deepest waters are at 4 °C (this is due to the fact that water is the densest at 4 °C). There is no mixing between water layers at different temperatures, thus no exchange of oxygen from the upper layers (where there's photosynthesis) to lower layers. But in winter seasons, it happens (unless it's global warming) that the air temperature reaches at least 4 °C for a sustained period of time. In that case, the surface water level also cools down to 4 °C, so there's no more layers, everyone is the same and can mix again! So in these conditions the water circulates through all depths of the lake, nutrients can be exchanged, and oxygen can be brought to the deepest parts of the lake. That's the **turnover**.

With global warming, average temperatures at the Bodensee are rising as well. In Constance, the current average trend shows an [increase of mean yearly temperatures of almost **2 °C in 4 decades**](https://www.meteoblue.com/en/climate-change/konstanz_germany_2885679) (!). Episodes of [winters too mild for a complete turnover are recurrent](https://www.lubw.baden-wuerttemberg.de/-/bodensee-winter-war-fur-eine-durchmischung-nicht-kalt-genug), last complete turnover going back to 2017/2018 (I am writing this during winter 2022/2023, so 🤞!). And with rising temperatures, well.. you get the picture.



**Action stations!** Shitty waters in a place that is developing its lakeside mass tourism is not super profitable. So (well, that's my interpretation) an [international commission](https://www.igkb.org/start/) was created in 1959. Its responsibility, from then on, would be to monitor the water quality of the lake. In 1963 came the irrevocable diagnosis: phosphates were indeed the culprits, so we'd have to reduce the input of phosphorus into the lake [^schindler]. In the 70s (while the Constance Lake whitefish was agonizing), sewage treatment plants were created, limits on phosphates contained in detergents set and incentives to extensify agriculture in the catchment area put in place, to reduce the amount of phosphate and nitrate dumped per unit of surface.

<div class="img-with-text" style="float:none">
<img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Bodensee_Phosphorkonzentration_1951-2005.svg" alt="Phosphorus concentration in the upper part of lake Constance, By Lax, CC BY-SA 3.0" width="400" title="Phosphorus concentration in the upper part of lake Constance"/>
<p>
<font size="2"  color="#D67A00">Phosphorus concentration in the upper part of lake Constance <br> <a href=" https://commons.wikimedia.org/w/index.php?curid=15921715" style="color: #D67A00"><i>by Lax, CC BY-SA 3.0</i></a></font>
</p>
</div>


The efforts - *and the billions invested* - paid eventually. After its peak value in the early 80s, phosphorus concentrations started decreasing slowly and finally [reached their pre-eutrophication levels in 2007](https://www.igkb.org/fileadmin/user_upload/dokumente/aktuelles/Faktenblaetter/2018-10-18_IGKB_Faktenblatt_Phosphor.pdf). The lake (re-)oligotrophied [^stich].

**Which goes to show that where there's a will there's a way...** but it has to be already too late. 

While managing adverse effects of a rather local pollution event (catchment area of about 100 km x 100 km) turned up to be doable, the time between the onset of drastic measures and a complete recovery of the original *trophic state* was long: at least 20 years. Imagine how long it might take for a thing of  (46 000 times bigger!) 510 000 000 square km.



---

[^vonlanthen]:  P. Vonlanthen et al. [Eutrophication causes speciation reversal in whitefish adaptive radiations](https://doi.org/10.1038/nature10824). 2012.
[^schindler]:  D.W. Schindler. [The dilemma of controlling cultural eutrophication of lakes](https://doi.org/10.1098/rspb.2012.1032). 2012.
[^stich]:  H.B. Stich & A. Brinker. [*Oligotrophication outweighs effects of global warming in a large, deep, stratified lake ecosystem*](https://doi.org/10.1111/j.1365-2486.2009.02005.x). 2010.

[^rhymer]: Review on introgressive hybridization, with focus on introduced species: J.M. Rhymer and D. Simberloff. [*Extinction by hybridization and introgression*](https://doi.org/10.1146/annurev.ecolsys.27.1.83). 1996. 
[^rosenzweig]: M.L. Rosenzweig. [*Loss of speciation rate will impoverish future diversity*](https://doi.org/10.1073/pnas.101092798). 2001.
[^barnett]: A. Barnett & B.E. Beisner. [*Zooplankton biodiversity and lake trophic state: explanations invoking resource abundance and distribution.*](https://doi.org/10.1890/06-1056.1) 2007
[^wang]: H. Wang et al. [*Eutrophication causes invertebrate biodiversity loss and decreases cross-taxon  congruence across anthropogenically-disturbed lakes*](https://doi.org/10.1016/j.envint.2021.106494). 2021.



