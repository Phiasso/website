---
layout: post
title: "Qu'est-ce qu'on mange au lac de Constance ? pte.1"
permalink: "/fr/bodensee-fishing/"
date:   2023-02-21 10:57:00 +0200
categories: ecology
tags: bodensee lac constance pêche felchen lavaret coregone
lang-ref: fishing
lang: fr
published: true
---

## Histoires d'écologie du lac de Constance II.1 

### La pêche: une tradition millénaire

Les pêcheurs du lac de Constance se targuent de [perpétuer une tradition](https://www.bodensee-fischer.de/fisch-informationen/bodenseefische/) centenaire. Et c'est vrai. 

On peut même sans doute dire millénaire, dans la mesure où les premières habitations retrouvées autour du lac remontent au Néolithique. À l'époque de ces [cités lacustres](https://fr.wikipedia.org/wiki/Sites_palafittiques_pr%C3%A9historiques_autour_des_Alpes), la pêche était déjà un important moyen de subsistance. Sans remonter jusqu'à la préhistoire, quelques éléments de l'époque précédant la révolution industrielle valent le coup d'être considérés pour observer l'évolution des pratiques et évaluer la pression que les humains exercent sur les sources de nourriture de son écosystème. 

<div class="img-with-text" style="float:none">
<img src="https://upload.wikimedia.org/wikipedia/commons/c/c4/Bodensee_Karte_Merian_1672_color.jpg" alt="Lac de Constance. Gravure sur cuivre coloriée, vers 1640." title="Lac de Constance. Gravure sur cuivre coloriée, vers 1640."/>
<p>
<span class = "legende"><i>Lacus Podamicus.</i> Lac de Constance. Gravure sur cuivre coloriée, vers 1640. (<a href="https://commons.wikimedia.org/wiki/File:Bodensee_Karte_Merian_1672_color.jpg" style="color: #D67A00"><i>source: wikimedia commons</i></a>)</span>
</p>
</div>


#### La pêche de 1350 à 1900 [^zeheter]

Au bas Moyen Âge, bien que l'intégralité de la région du lac de Constance ait été incluse dans le Saint-Empire romain germanique (le Premier Reich), le territoire autour du lac était bien morcelé. Il était organisé — si on simplifie très grossièrement — en seigneuries. Et cette organisation était à peu près stable jusqu'à la Révolution française (oui, 🐓) et l'arrivée de Napoléon, début du XIXᵉ, qui a décidé de tout dégommer sur son passage. Bref, comme le territoire était fragmenté, il fallait s'organiser entre qui a le droit de pêcher et où. D'autant que, toute la surface du lac n'était pas entièrement découpée en titres de propriété, certaines zones en étaient considérées comme bien communaux, auxquels tout pêcheur avait accès. 

<div class="img-with-text" style="float:right">
<img src="https://upload.wikimedia.org/wikipedia/commons/7/7b/Fischer.jpg" alt="Blason de la corporation des pêcheurs" title="Blason de la corporation des pêcheurs" width="240px"/>
<p align="center">
  <span class = "legende">Blason de la corporation des pêcheurs<br> 1895, Leipzig (<a href="https://commons.wikimedia.org/wiki/File:Fischer.jpg" style="color: #D67A00"><i>wikimedia commons</i></a>)</span>
</p>
</div>

**Les guildes de pêcheurs.** Au XIVᵉ siècle, comme beaucoup d'autres corps de métiers, les pêcheurs professionnels commencent à s'organiser en corporations (ou guildes). Ce mode d'organisation devient même exclusif, et ce jusqu'au milieu du XIXᵉ, puisque pour pouvoir vendre son poisson sur le marché, il fallait faire partie de la guilde des pêcheurs (prendre sa canne à pêche pour aller choper un peu de poisson pour la famille était évidemment autorisé). La guilde servait, en outre, comme médiatrice entre les pêcheurs et les autorités locales, et mettait en place des règlements et des contrats de pêche de façon autonome ou en lien avec ces dernières.

**Les poissons du lac.** Mais qu'est-ce qu'on peut bien pêcher dans le lac de Constance ? Eh bien, ce lac est l'habitat typique de certains salmonidés, comme le lavaret (ou corégone), la truite, l'omble et l'ombre. Les lavarets (car il y en a plusieurs types) sont les plus emblématiques du lac de Constance. On les appelle *Felchen* en allemand, et plus spécifiquement *Blaufelchen*, *Gangfisch*, *Sandfelchen* et *Kilch* pour les quatre espèces représentées (respectivement *wartmanni*, *macrophtalmus*, *arenicolus*, *gutturosus*)[^coregones]. Ah non, sauf le Kilch, lui, a disparu à jamais ([voir I](/fr/bodensee-eutrophication/#evolution)). Ce sont des espèces pélagiques, c’est-à-dire qu'on les trouve au large, dans les profondeurs du lac. En plus des salmonidés, trouve dans le lac tout un tas d'autres poissons dits communs, moins sélectifs sur la profondeur, comme la perche, la tanche, le rotengle, le gardon, le brochet, la lotte, la carpe et même l'anguille [^hierarchie]! 

<div class="img-with-text" style="float:right">
<img src="https://upload.wikimedia.org/wikipedia/commons/d/da/Konstanzer_Richental_Chronik_Verkauf_von_von_Fischen%2C_Fr%C3%B6schen_und_Schnecken_25r.jpg" alt="au marché" title="au marché" width="270px"/>
<p align="center">
  <span class = "legende">Au marché d'escargots et de poisson<br> pendant le Concile de Constance.<br>Ulrich von Richental, vers 1464.<br> (<a href="https://commons.wikimedia.org/wiki/File:Konstanzer_Richental_Chronik_Verkauf_von_von_Fischen,_Fr%C3%B6schen_und_Schnecken_25r.jpg" style="color: #D67A00"><i>wikimedia commons</i></a>)</span>
</p>
</div>

**Le poisson, un aliment de carême.** À cette époque, la consommation de poisson avait son importance, surtout dans la mesure où c'était un mets autorisé pendant le carême, période de l'année où l'Église interdisait la consommation de produits gras et d'origine animale, et qui pouvait durer jusqu'à 1/3 de l'année. Et il est bien connu que le poisson n'est pas un animal mais un "légume de cours d'eau" (["fluvium legumine"](https://www.bkz.de/nachrichten/legenden-umranken-diese-speise-69115.html) qu'ils disaient, les moines). C'est le castor qui, lui, est l'animal de l'eau et donc c'est.. un poisson (? 🤔).. Enfin tout n'est que question d'interprétation ! Avec cette pirouette interprétative-ci, les poissons ne tombaient pas sous l'interdiction de carême, et ça les arrangeait bien, les religieux.

**Techniques de pêche.** Alors, pour pêcher de façon efficace, il y avait tout un tas de techniques super élaborées, largement suffisantes pour vider tout le lac de ses légumes. Y avait les équivalents de la [nasse](https://fr.wikipedia.org/wiki/Nasse), du [filet maillant](https://fr.wikipedia.org/wiki/Filet_maillant), de la [senne](https://fr.wikipedia.org/wiki/P%C3%AAche_%C3%A0_la_senne), du [vervet](https://fr.wikipedia.org/wiki/Filet_de_p%C3%AAche#Verveux), (pour les profanes comme moi, tout ça, ça reste des filets, hein).. pas trop la canne à pêche, ça c'était pour le pêcheur du dimanche. Par exemple, la *Klusgarn*, une senne de 80m de long et jusqu'à 30 de haut, fixée sur deux longues cordes était plongée dans les profondeurs du lac pour pêcher le lavaret. Elle avait un rendement variable entre 0 et 10 lavarets par prise, mais qu'on pouvait compenser en tentant son coup jusqu'à 60 fois en une journée (à condition de passer 15h sur l'eau). Le *Landwatt* était un filet lancé depuis la berge, des mêmes dimensions à peu près que le précédent, mais qui était orienté, lui, sur la brème  — poisson qui préfère les eaux littorales aux eaux profondes. Le *Landwatt* pouvait régulièrement avoir des rendements de plusieurs tonnes. Le plus spectaculaire fut cependant le *Gangfischwatt*, un filet mesurant jusqu'à 120m de long et 4 de large, formé à partir des filets individuels de 18 pêcheurs, qui devaient s'y mettre à 18 pour pouvoir le manipuler. Le *Gangfischwatt*, comme son nom l'indique, était destiné à choper le *Gangfish* (l'un des lavarets) pendant la *fraie*, qui est la période de reproduction des poissons. Car à ce moment-là — ce moment-là c'est le début de l'hiver — le *Gangfisch*, qui d'habitude zone dans les profondeurs, remonte à la surface près des côtes, pour faire une danse nuptiale lors de laquelle les œufs pondus sont arrosés par des jets de sperme. Tombant dans les profondeurs, les œufs, s'ils sont fécondés, se développent l'hiver durant à 4°C jusqu'à éclore au printemps. Alors tous ces poissons remontés à la surface, occupés à danser font une proie bien facile pour les pêcheurs, qui en profitent pour les choper dans leurs grands filets. Là, le timing étant très important (la danse dure à peine quleques jours), alors le rendement était variable, mais le record aurait été de 150 000 *Gangfische* à Ermatingen en 1817. Ils ont dû en manger du poisson, après ça !

<div class="row">
  <div class="column">
    <img src="/images/zeheter_1.jpeg" alt="Gangfischwatt" style="width:100%">
    <p align="center"><span class="legende">Gangfischwatt. Y en a bien 18 !</span>
    </p>
  </div>      
  <div class="column">     
    <img src="/images/zeheter_2.jpeg" alt="Landwatt (Vorarlberg, vers 1930)" style="width:100%">   
    <p align="center"><span class="legende">Landwatt (Vorarlberg, vers 1930)</span>
    </p>
  </div>   
  <div class="column">     
    <img src="/images/zeheter_3.jpeg" alt="Klusgarn (Obersee, vers 1930)" style="width:100%"> 
    <p align="center"><span class="legende">Klusgarn (Obersee, vers 1930)</span>
    </p>
  </div> 
</div> 

<small>illustrations : M. Zeheter </small> [^zeheter]

Bon, tout ça pour dire, que les techniques de pêche étaient efficaces, même si le rendement pouvait en être variable.

**La durabilité au Moyen Âge.** Il existait cependant déjà des restrictions sur les méthodes de pêche : des règlements sur les mailles des filets ; sur la fréquence d'utilisation de telle ou telle méthode en fonction de son efficacité et de sa sélectivité sur une espèce ; des réglementations sur la maille (taille minimale du poisson pour pouvoir le pêcher) ; ou sur la saison, en fonction de la période de reproduction d'une espèce. Car si on profitait de l'apparition des lavarets en surface pour les pêcher, parce qu'ils étaient autrement difficiles à pécho, on interdisait de pêcher la perche lors de sa période de fraie, car là c'était quand même un peu trop facile. Tout ceci pour éviter la surpêche et assurer une sauvegarde durable de la "ressource poisson".

S'il est difficile de dire quels étaient les stocks de poissons commercialement intéressants dans le lac à ces époques, les restrictions et les plaintes de pêcheurs témoignent qu'ils pouvaient être pour le moins fluctuants. Il est à noter que les conditions climatiques (la période discutée s'étend pile sur le [petit âge glaciaire](https://fr.wikipedia.org/wiki/Petit_%C3%A2ge_glaciaire) !) pouvaient également jouer un rôle sur la quantité de poissons pêchés. Mais l'application de réglementations très strictes étant concomitantes avec les plus grands accroissements démographiques dans la région (XIIIᵉ, XIVᵉ et XVIIIᵉ) il est possible que l'intensité de la pêche ait, déjà à l'époque, pu avoir un effet sur la disponibilité de la ressource poisson. 

On peut d'ailleurs citer au moins un épisode de surpèche au XIVᵉ siècle, mais dans un plus petit lac pré-alpin, le *Zeller See* (région de Salzburg). Les quotas de 27 000 lavarets par an ont fait s'effondrer leur population en moins d'une génération humaine, et les brochets introduits pour les compenser ont décimé la population de truites. S'en est suivie une interdiction de pêche de 3 ans ainsi que des règlementations sévères sur les filets, zones et périodes de pêche. Il est fort à parier que la grande taille du lac de Constance lui a permis d'éviter telle catastrophe. Et s'il n'y a pas eu d'impact durable sur les populations de poissons, c'est sans doute grâce à un contrôle par les autorités et les guildes elles-mêmes. 

**Une histoire de pouvoir.** Il est à noter que, les guildes des pêcheurs étaient l'une des corporations les plus contrôlées (bien plus que les boulangers ou les bouchers, par exemple). Dans son livre, M. Zeheter estime que de cette façon, les autorités pouvaient exercer leur pouvoir bien au-delà des limites des fiefs, et ainsi étendre leur influence sur les eaux communes du lac.



#### 20ᵉ siècle jusqu'à nos jours

**Le déclin des corporations.** À la fin du XIXe siècle, la profession a connu des changements majeurs. D'abord par une réduction de leur autonomie politique : avec le déclin des guildes au XIXe dû à la libéralisation du commerce, ils ne faisaient plus partie de grandes structures qui pouvaient porter leur voix (syndiquez-vous !) mais étaient atomisés. Aussi, avec la création d'[instituts limnologiques](https://www.lubw.baden-wuerttemberg.de/wasser/institut-fuer-seenforschung), instituts de recherche spécialisés dans l'étude de l'écosystème lacustre avec une visée d'exploitation durable de la ressource poisson, ils ont perdu leur légitimité dans la production de connaissances empiriques. On a en effet, estimé que, pour établir réglementations et restrictions, les pêcheurs eux-mêmes auraient un conflit d'intérêt entre la conservation durable de la ressource qu'ils exploitent et se faire de la grosse thunasse à court terme.

**Licences de pêche individuelles.** Entre 1914 et 1934, le nombre maximal de licenses de pêche est passé de 435 à 218 afin de réduire la concurrence entre les pêcheurs et leur permettre de vivre de leur activité. Et depuis, le nombre de pêcheurs n'a cessé de diminuer, parfois par manque de pêcheurs, qui préféraient s'orienter vers les industries naissantes dans la région pour leur gagne-pain, parfois par limitation des autorités.

<div class="img-with-text" style="float:right">
<img src="/images/Bodennetz-auslegen.jpg" alt="Pêcheur sur le lac de Constance" title="Pêcheur sur le lac de Constance" width="300px"/>
<p align="center">
  <span class = "legende">Pêcheur sur le lac de Constance <br> (<a href="https://www.bodensee-fischer.de/medienberichte/bilder-vom-see-und-der-fischerei/" style="color: #D67A00"><i>bodensee-fischer.de</i></a>)</span>
</p>
</div>

<a name="evolution-techniques"></a>

**Les techniques de pêche** ont globalement été perpétuées au fil des siècles, mais rendues encore plus efficaces avec l'industrialisation et la motorisation. Avec le passage à la maille en nylon, certains filets comme la *Klusgarn* ont été jugés *trop* efficaces et pas assez sélectifs (ça ratissait tout sur son passage), et ont été interdits [^klusgarn]. Aujourd'hui, les pêcheurs professionnels n'utilisent que des [filets maillants](https://fr.wikipedia.org/wiki/Filet_maillant), aux mailles assez larges de façon à piéger uniquement les poissons adultes ayant atteint une certaine taille (ou maille). Bien qu'une telle mesure puisse paraître salutaire pour éviter de pêcher les juvéniles et leur laisser le temps d'arriver à maturité sexuelle pour pouvoir se reproduire, et ainsi pérenniser la population, il a été montré que de telles mesures pouvaient exercer une pression sélective sur certaines populations de poissons de mer, en sélectionnant les individus de petite taille et participant ainsi à une perte de la diversité génétique d'une population [^jorgensen]. Cela n'a cependant pas été observé sur nos populations de lavarets, mais doit continuer à être surveillé [^gum].

<a name="baisse-rendement"></a>

Malgré un lac redevenu oligotrophe ([voir I](/fr/bodensee-eutrophication/#reoligotrophisation)) ; malgré une surveillance accrue des populations de lavarets, et même une supplémentation artificielle des eaux du lac en larves de poissons incubées dans des [alevinières](https://www.ibkf.org/en/brut-und-aufzuchtanlagen/) ; malgré un contrôle des techniques de pêche et du nombre pêcheurs (qui par ailleurs a été réévalué à 80 sur tout le lac en 2020) ; malgré tout ça, les rendements de lavarets pêchés dans le lac de Constance ne cesse de baisser. On a même atteint le record du rendement le plus bas jamais obtenu : [20 tonnes de lavarets en 2022 seulement](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/fangzahlen-bei-bodenseefelchen-auf-rekordtief-100.html)[^rendement2022], alors qu'on était à plus de [800 tonnes à la fin des années 90](https://www.ibkf.org/wp-content/uploads/2022/06/IBKF-2022_Blaufelchenbericht.pdf). 

Alors, les pêcheurs sont inquiets, et à raison. Certains estiment que le lac est "trop propre" et que le lavaret n'a plus assez à manger (non, il ne mange pas nos saletés..), d'autres pointent du doigt l'épinoche à trois épines, un petit poisson récemment introduit dans le lac qui dévorerait tous les œufs du lavaret, et les derniers accusent le cormoran, dont la population augmente autour du lac, de [dévorer les 300 tonnes de poisson](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/vorstudie-empfiehlt-gemeinsame-aktionen-gegen-kormoranen-am-bodensee-100.html) qui sont normalement dues aux pêcheurs. En fait, comme souvent, c'est plus compliqué que ça, alors j'y consacre la [deuxième partie de "Qu'est-ce qu'on mange sur le lac de Constance ?"](/fr/bodensee-felchen/).

En tout cas, déjà en 2012, 50% des lavarets consommés dans la région étaient importés d'Italie, de Finlande, d'Islande, du Canada... sans que les consommateurs en aient conscience [^baer].  Alors, continuer à développer un tourisme de masse tout en faisant mousser la marque "poisson du *Bodensee*" paraît légèrement schizophrène ou pour le moins malhonnête.

[⥽ Lire la suite ! ⥼](/fr/bodensee-felchen/)



---



[^zeheter]: Toute cette partie est basée sur le livre de **Michael Zeheter** [Die Ordnung der Fischer. Nachhaltigkeit und Fischerei am Bodensee (1350-1900)](http://dx.doi.org/10.7788/boehlau.9783412217549) (L'organisation des pêcheurs. Soutenabilité et pêche sur le lac de Constance de 1350 à 1900). 2014.
[^coregones]: La disctinction entre les quatre espèces est débattue, certains considérant qu'il ne s'agit que d'une seule et même espèce, très polymorphe, le Corgenus *lavaretus* avec des sous-espèces ou écotypes.
[^hierarchie]: Évidemment il y avait (et il y a toujours) une hiérarchie dans les poissons à consommer, et donc économiquement plus intéressants. Dans l'ordre du plus noble au moins noble: anguille > truite, lavaret, perche, ombre, lotte, tanche, carpe > barbeau, brème, lavaret arénicole > chevesne, ablette, vairon
[^klusgarn]: Interdiction en 1967 pour la Klusgarn. https://www.schule-bw.de/faecher-und-schularten/gesellschaftswissenschaftliche-und-philosophische-faecher/landeskunde-landesgeschichte/module/epochen/umweltgeschichte/bodensee/photoalbum_bodensee/b43.png **FAO**. [Techniques de pêche](https://www.fao.org/3/AA044E/AA044E06.htm) [Ctrl]+[F] "Klusgarn".
[^jorgensen]: L'idée est que les individus à croissance rapide auraient une chance réduite de se reproduire, en raison d'une mortalité par pêche plus élevée, et que cette pression qu'exerce la pêche sur la taille peut avoir un effet négatif sur le potentiel de croissance de la population au fil du temps. **C. Jørgensen et al.** [Managing Evolving Fish Stocks](https://sci-hub.st/https://doi.org/10.1126/science.1148089). 2007.
[^gum]: **B. Gum et al.** [Genetic diversity of upper Lake Constance whitefish Coregonus spp. under the influence of fisheries: a DNA study based on archived scale samples from 1932, 1975 and 2006](https://sci-hub.st/https://doi.org/10.1111/jfb.12393). 2014.
[^rendement2022]: Le stock de lavaret était si bas en 2022 que la pêche pendant la fraie, début décembre, a été [annulée par l'IBKF](https://www.ibkf.org/wp-content/uploads/2022/12/IBKF-Pressemitteilung-Felchenlaichfischerei-2022.pdf), l'orgaisme international qui régule la pêche sur le lac de Constance, ce qui participe aussi au faible rendement annuel.
[^baer]: **J.Baer et al.** Managing Upper Lake Constance fishery in a multi-sector policy landscape: Beneficiary and victim of a century of anthropogenic trophic change. Chap. 3 de [Inter-sectoral governance of inland fisheries](http://toobigtoignore.net/research-highlights-1/e-book-inter-sectoral-governance-of-inland-fisheries/). 2017.

