---
layout: page
title: About
permalink: "/about/"
lang: en
lang-ref: about
---

Sophia Karpenko  

Postdoc at the Max Planck Institute for Animal Behavior, Department for Collective Behavior.  

[> download full CV (pdf) <](/files/cvSK_en.pdf)

## Publications

### Thesis
*Light-seeking navigation in zebrafish larva : from behavior to neural circuits*, PhD thesis, 2020. S.Karpenko.  
PDF [link](files/PhDThesis_KARPENKOSophia_20210312.pdf)  
Theses.fr database: [theses.fr/en/s195224](https://theses.fr/en/s195224)

### Peer-review

*Thermal modulation of exploratory behaviorsreveals constrains on individual variability withindefined kinematic behaviors*, BMC Biology (2021). G. Le Goc, J. Lafaye, S. Karpenko, V. Bormuth, R. Candelier, G. Debrégeas.  
DOI : [10.1186/s12915-021-01126-w](https://doi.org/10.1186/s12915-021-01126-w)

*From behavior to circuit modeling of light-seeking navigation in zebrafish larvae*, Elife, 2020.  
S. Karpenko, S. Wolf, J. Lafaye, G. Le Goc, T. Panier, V. Bormuth, R. Candelier, G. Debrégeas  
DOI : [10.7554/eLife.52882](https://doi.org/10.7554/eLife.52882)

*Sensorimotor computation underlying phototaxis in zebrafish*, Nature Communication, 2017.  
S. Wolf , A. Dubreuil , T. Bertoni , U.L. Böhm , V. Bormuth , R. Candelier , S. Karpenko , D.G.C. Hildebrand , I. H.
Bianco , R. Monasson , G. Debrégeas  
PDF [link](https://www.labojeanperrin.fr/Documents/Publications/148.pdf)  
DOI : [10.1038/s41467-017-00310-3](https://doi.org/10.1038/s41467-017-00310-3)

**From former thematics** :  
*A bacterial cell factory converting glucose into scyllo-inositol, a therapeutic agent for Alzheimer’s disease*, Communications Biology, 2020.  
C. Michon, CM. Kang, S. Karpenko, K. Tanaka, S. Ishikawa & Ken-ichi Yoshida  
DOI : [10.1038/s42003-020-0814-7](https://doi.org/10.1038/s42003-020-0814-7)
