---
layout: post
title: "What do we eat at lake Constance?"
permalink: "/bodensee-food/"
date:   2023-02-01 13:12:00 +0200
categories: ecology
tags: bodensee lake constance food
published: false
lang: en
lang-ref: flc

---

*Little ecological stories from lake Constance: Part 2 or 3*

Fish
- Introgression from extinct species facilitates adaptation to its vacated niche https://onlinelibrary.wiley.com/doi/10.1111/mec.16791
- Rapid niche expansion by selection on functional
genomic variation after ecosystem recovery https://doi.org/10.1038/s41559-018-0742-9

