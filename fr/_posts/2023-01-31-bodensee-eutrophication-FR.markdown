---
layout: post
title: "Épisode d'eutrophisation du lac de Constance"
permalink: "/fr/bodensee-eutrophication/"
date:   2023-01-31 15:12:00 +0200
categories: ecology
tags: bodensee lac constance eutrophisation
lang-ref: beutroph
lang: fr
published: true
---

<img src = "/images/bodensee/cygne-mainau.JPG" width="100%" title="Vue de l'île de Mainau (oct. 2021)">



## Histoires d’écologie du lac de Constance I.

<font color="#00b07c"><b>La pollution massive de l’un des lacs les plus propres d’Europe</b> <br><small>(mais bon, <a href="https://en.wikipedia.org/wiki/Eutrophication#Extent_of_the_problem">c’est normal</a>).</small></font>



### Lac de Constance

C’est l’extrême sud de l’Allemagne, à la frontière avec la Suisse et l’Autriche. C’est là que se trouve l’un des plus grands lacs d’Europe, le plus grand d’Allemagne, en tout cas : le lac de Constance <small>(pour boucler la question de qui a le plus grand dans l’absolu, on peut aller voir [ici](https://fr.wikipedia.org/wiki/Liste_de_lacs_d%27Europe))</small>.

<div class="img-with-text" style="float:right">
<img src="/images/Einzugsgebietskarte_bodensee_723x1024.jpg" alt="Bassin versant du lac de Constance" width="300" title="Lake Constance catchment area"/>
<p align="center">
  <span class="legende">Bassin versant du lac de Constance (11 000 km²)<br> (<a href="https://www.canyonauten.de/blog/wasserstand-bodensee-pegel-konstanz/" style="color: #D67A00"><i>source image</i></a>)
  </span>
</p>
</div>


Le lit du lac a été creusé par le glacier du Rhin lors de la dernière période glaciaire ([glaciation de Würm](https://fr.wikipedia.org/wiki/Glaciation_de_W%C3%BCrm)), qui s’est terminée il y a environ 10 000 ans, démarrant ainsi l’époque Holocène. À quelques milliers d’années près, cela correspond aussi à la période Néolithique, qui a laissé quelques traces sur les abords du lac sous la forme de vestiges de [maisons sur pilotis](https://www.pfahlbauten.eu/fr/le-musee-des-palafittes/promenade-virtuelle.html) de cités lacustres. 

Le *Lacus Constantinus* a été baptisé à l’occasion du [concile de Constance](https://fr.wikipedia.org/wiki/Concile_de_Constance) au XVᵉ siècle. Mais son nom d’usage en allemand est *Bodensee* ("Bodmansee" = le lac de Bodman, une petite commune du Nord-Ouest du lac supérieur, qui avait une plus grande influence au Moyen Âge).

Les eaux de la partie supérieure du lac (l’*Obersee*) sont officiellement internationales, malgré une tentative de la Suisse au XIXᵉ siècle, de tracer une frontière en plein milieu, pour délimiter son territoire. En effet, les duchés de Bade, de Bavière et l’Autriche l’avaient envoyée paître (sauf pour certaines zones spécifiques autour de la ville de Constance). Le lac supérieur est donc jusqu’à aujourd’hui un [condominium](https://fr.wikipedia.org/wiki/Condominium#Territoire_lacustre), bon.. sauf pour les Suisses qui l’ont quand même [tracée, leur frontière](https://www.bodensee-geodatenpool.net/file/16.pdf).

Au cours des XIXᵉ et XXᵉ siècles, la population humaine aux abords du lac a massivement augmenté, [comme dans une grande partie de l’Europe](https://ourworldindata.org/grapher/population?time=1100..latest&country=DEU%7EEurope%7EEuropean+Union+%2827%29). La ville de Constance, la plus peuplée aujourd’hui, est passée d’une constante [de 5 000 habitants pendant des siècles à plus de 75 000 habitants en 200 ans](https://de.wikipedia.org/wiki/de:konstanz?uselang=en#Bev%C3%B6lkerungsentwicklung). La région du lac a subi une grosse intensification de son activité agricole, une urbanisation et une artificialisation de son bassin versant. Et ce fut non sans conséquences pour l’écosystème lacustre.



### Eutrophisation en milieu aquatique

**Changement d’état trophique.** Dans la seconde moitié du XXᵉ siècle, le lac de Constance a souffert d'un grand épisode de pollution, le faisant basculer d’un état [*oligotrophe*](https://fr.wikipedia.org/wiki/Oligotrophe), pauvre en nutriments, caractéristique des grands lacs péri-alpins, à un état [*eutrophe*](https://fr.wikipedia.org/wiki/Eutrophisation). L’état trophique d’un milieu est caractérisé par la disponibilité de nutriments essentiels pour la croissance des [producteurs primaires](https://fr.wikipedia.org/wiki/Autotrophie) que sont les plantes et les algues, ces organismes qui mangent de la lumière et du CO<sub>2</sub> pour produire du dioxygène. Plus un milieu est riche en ces nutriments — l’azote et le phosphore pour les lacs d’eau douce — plus la croissance et la présence de plantes, d’algues, de phytoplancton seront importantes, voire excessives. L’eau, au lieu d’être limpide avec peu de particules en suspension, devient verte et opaque.

À noter donc que, bien qu’il existe des milieux naturellement *eutrophes*, donc riches en nutriments, le terme d’*eutrophisation* désigne un processus de dégradation d’un milieu par un apport excessif de nutriments, donc une pollution.

**Prolifération algale par excès de phosphore.** Dans notre cas, les concentrations de phosphore dans le lac de Constance (élément normalement limitant, présent en faibles concentrations) ont été [multipliées par 10 entre 1950 et la fin des années 1970](https://www.igkb.org/fileadmin/user_upload/dokumente/aktuelles/Faktenblaetter/2018-10-18_IGKB_Faktenblatt_Phosphor.pdf). Cette pollution au phosphore a été causée par l’augmentation considérable de rejet des eaux usées (urbanisation croissante, je rappelle) et le lessivage d’engrais des champs entourant le lac, l’usage desquels a explosé après la seconde guerre mondiale. La matière fécale, les lessives et les engrais regorgeant de phosphore, la situation est devenue complètement merdique dans les années 1970: les autorités ont dû interdire l’accès à certaines plages en haute saison, car risque accru de choléra et polio.

<a name="ecologie"></a>

**Apparition de zones mortes.** On pourrait penser que s’il y a davantage de nutriments dans l'eau, il aurait, certes, davantage d’algues, mais donc davantage de nourriture pour les herbivores comme les coquillages et crustacés. Les populations de ces derniers grandiraient et seraient une source de nourriture pour les poissons molluscivores et planctivores, et finalement ce seraient les populations de poissons qui grandiraient ! Mais c’est plus compliqué que ça. Les [efflorescences algales](https://fr.wikipedia.org/wiki/Efflorescence_algale) en surface de l’eau réduisent la quantité de lumière qui pénètre dans les parties plus profondes du lac et empêchent ainsi d’autres organismes photosynthétiques (le petit phytoplancton) de manger la lumière. Alors ils meurent. Et les algues de l’efflorescence finissent par mourir aussi. Tout ce beau monde tombe dans le fond du lac, où il est décomposé par des microorganismes. Mais les microorganismes utilisent beaucoup d’oxygène lorsqu’ils mangent les débris d’algues, alors plus il y a de débris, plus ils consomment d’oxygène et moins il en reste pour les autres organismes qui vivent au fond de l’eau et ont besoin de respirer... alors ces derniers meurent aussi. Ceci créé des zones pauvres en oxygène, dites [hypoxiques](https://fr.wikipedia.org/wiki/Hypoxie#%C3%89cologie_aquatique). Et lorsque l’oxygène vient à manquer complètement, elles deviennent zones mortes où plus rien ne peut vivre. 

<a name="evolution"></a>

**Extinction d'une espèce de poisson.** Au moins une espèce (emblématique) a irrémédiablement souffert des conditions hypoxiques dans le fond du lac : le lavaret (ou corégone) du lac de Constance, *[Coregonus](https://fr.wikipedia.org/wiki/Coregonus) gutturosus*, appelé *Kilch* en allemand. Ses œufs, déposés dans les profondeurs, ne parvenaient plus à se développer sans oxygène, ce qui a conduit à l’extinction complète de l’espèce dans les années 70. 

Mais il reste quand même une bonne nouvelle : *C. gutturosus* est toujours parmi nous, en quelque sorte. Pas que dans nos cœurs, non, mais aussi dans le matériel génétique d’autres espèces du genre corégone[^vonlanthen], par exemple chez [*C. macrophthalmus*](https://fr.wikipedia.org/wiki/Coregonus#Esp%C3%A8ces), une espèce certes menacée.. mais qui survit ! 

<div class="img-with-text" style="float:right">
	<a href="https://seewandel.org/en/p4-eng/">
  <img src="/images/Bodensee_Felchenarten_Foto_Oliver_Selz_Uni_Bern__Eawag.jpg" alt="Corégones" width="330" title="Les Corégones du lac de Constance"/>
  </a>
  <p align = "center">
    <span class="legende">
      Les lavarets (corégones) du lac de Constance.
      <br> (A) <i>C. arenicolus</i> (B) <i>C. macrophthalmus</i><br> 
      (C) Le défunt <i>C. gutturosus</i>. † <b>RIP</b> (D) <i>C. wartmanni</i><br>
      <i>(Image: projet de recherche Seewandel)</i></span>
  </p>
</div>  


L’extinction du *kilch* ne s’est, en effet, pas faite que par le biais d’un déclin démographique de sa population, mais aussi par un processus appelé *[introgression](https://fr.wikipedia.org/wiki/Introgression)* par hybridation avec une espèce apparentée. Ce qui s’est passé, c’est que *C. gutturosus* s’est mis à se reproduire avec *C. macrophtalmus*, ce qui a engendré des *hybrides* des deux espèces. Ces hybrides se sont ensuite reproduits avec des individus des deux espèces parentes, mais moins avec *gutturosus*, puisque sa population était en déclin. De génération en génération, cela a mené à l’enrichissement de l’ADN de *macrophtalmus* avec l’ADN de *gutturosus*, et d’un point de vue évolutif à une *déspéciation* (le processus inverse de l’[apparition d’une espèce](https://fr.wikipedia.org/wiki/Sp%C3%A9ciation)) de *gutturosus* [^rhymer]. 

La déspéciation pose problème en ce qu’elle participe à une tendance plus globale de perte de la (bio)diversité au sein d’écosystèmes dégradés par l’homme [^rosenzweig]. Et cette tendance est très largement observable lors du processus d’eutrophisation, pas uniquement pour le cas des lavarets, mais aussi pour les communautés de plancton, pour ne citer que ça [^barnett],[^wang]. Et le truc, c’est que la perte de biodiversité c’est mauvais. Mais cela n’a rien d’évident, alors j’y reviendrai une autre fois.

<a name="retournement"></a>

**Retournement du lac**. Donc, l’oxygène ça peut être important pour ne pas mourir, voire s’éteindre à l’échelle de l’espèce. L’oxygène dans un lac provenant principalement de l’air et de l’activité photosynthétique des algues, il se concentre surtout en surface. Plus on va profond, plus on s’éloigne de la surface et moins il y a de lumière, donc moins de photosynthèse (rappelez-vous, les algues ça mange la lumière !), donc moins d’oxygène. 

Alors, déjà, on voit que plus un lac est profond, plus l’oxygène arrive difficilement dans le fond. Mais en plus, on a un obstacle supplémentaire, qu’est la différence de température entre la surface d’un lac et ses abîmes. En effet, à partir d’une certaine profondeur, on observe une stratification de la température des eaux d’un lac en saison chaude : la surface est à la température de l’air (20 °C par exemple), tandis que les eaux profondes sont à 4°C (car l’eau est la plus dense à 4°C). Il n’y a pas de brassage entre les couches de température différente et donc pas d’échanges gazeux, et pas de transfert d’oxygène de la surface vers les couches plus profondes. Cependant, en hiver, il arrive que la température de l’air soit suffisamment basse pour que la température de la surface du lac atteigne elle aussi les 4°C. Et alors, il n’y a plus de couches différentes, tout se retrouve à la même température et peut à nouveau se mélanger ! Dans ces conditions, c’est la teuf : l’eau circule dans toutes les profondeurs du lac, il y a un brassage, les nutriments peuvent être échangés et l’oxygène amené tout au fond. #*retournement*.

C’est donc ainsi, par le retournement, que l’oxygène arrive dans les profondeurs (max. 250m) du lac de Constance.

Avec le réchauffement climatique, les températures moyennes autour du lac augmentent. À Constance, la tendance moyenne montre une augmentation des températures moyennes annuelles de presque [2°C en 4 décennies](https://www.meteoblue.com/en/climate-change/konstanz_germany_2885679) (!). Et les épisodes d’hivers trop doux pour un retournement complet du lac se multiplient: le dernier datant de l’hiver 2017/2018 (on croise les doigts pour l’hiver 2022/2023). Alors si cette mode des températures qui augmentent se poursuit, ben... vous voyez le tableau. Et comme c’est une tendance générale des lacs en milieux tempérés, ça ne présage rien de bon pour les petites bêtes qui vivent dans les profondeurs [^lake-oxygen].

<a name="reoligotrophisation"></a>

<div class="img-with-text" style="float:right">
<img src="https://upload.wikimedia.org/wikipedia/commons/3/33/1PhosphoreBodensee.jpg" alt="Concentration de phosphore dans la partie sup. du lac de Constance, par Lax, CC BY-SA 3.0" width="350" title="Concentration de phosphore dans la partie sup. du lac de Constance"/>
<p align = center>
<span class = "legende">Évolution de la concentration de phosphore<br> dans la partie supérieure du lac de Constance<br> <a href="https://fr.wikipedia.org/wiki/Lac_de_Constance#/media/Fichier:1PhosphoreBodensee.jpg" style="color: #D67A00"><i>Lax, CC BY-SA 3.0</i></a></span>
</p>
</div>

**Branle-bas de combat !** De retour à nos eaux usées. De la merde dans un lac dont on essaye de développer l’attrait touristique, c’est pas fameux. Alors (mais ce lien de causalité, ce n’est que mon interprétation !), en 1959, a été créé une [commission internationale](https://www.igkb.org/start/) regroupant les 3 pays limitrophes du lac, plus le Liechtenstein qui fait partie du bassin versant. Sa responsabilité serait de surveiller la qualité de l’eau du lac et mettre en place des mesures d’assainissement. En 1963 est tombé le diagnostic : le coupable était bien le phosphore, il faudrait donc tout mettre en œuvre pour réduire la quantité de phosphore rejetée dans le lac [^schindler]. Dans les années 1970, pendant que le kilch agonisait, des milliards ont été débloqués pour installer des stations d’épuration sur le pourtour du lac, limité la quantité de phosphates autorisés dans les lessives et encouragé des pratiques d’élevage plus extensives dans le bassin versant, afin de limiter la quantité de nitrates et de phosphates rejetés par unité de surface.


Les efforts — et les milliards investis (5,4 milliards de $ US[^baer]) — ont porté leurs fruits. Après avoir atteint son maximum au début des années 80, le temps que le lac se décharge par le Rhin dans la mer du Nord, la concentration de phosphore s’est mise à baisser graduellement pour atteindre son [niveau de pré-eutrophisation en 2007](https://www.igkb.org/fileadmin/user_upload/dokumente/aktuelles/Faktenblaetter/2018-10-18_IGKB_Faktenblatt_Phosphor.pdf). Le lac s’est re-oligotrophié  [^stich].

Alors, **quand on veut on peut** ? Oui mais...

Gérer les conséquences négatives d’un événement de pollution relativement localisé (un bassin versant d’environ 100x100 km) peut s’avérer faisable. Mais il s’agissait, *in fine* de gérer *une seule* variable — le taux de phosphore. Les dégradations environnementales qu’on occasionne sont souvent multifactorielles et de fait plus complexes à "gérer". Il faut souligner également, que le retour à l’état trophique "originel" a mis du temps: une quarantaine d’années entre les premières mesures et le retour à la normale. Et cet interlude d’eutrophisation a impacté l’écosystème de façon irréversible, pour ne citer que l’extinction du kilch. Enfin, on remarquera que cet épisode a été étudié en prenant le système "lac de Constance" comme isolé du reste du monde : sans considérer les effets du déchargement de nutriments du lac sur son aval le Rhin, sur le delta Rhin-Meuse-Escaut aux Pays-Bas et sur la mer du Nord, chacun étant un écosystème particulier qui réagit différemment à ce genre de perturbation (oh l'euphémisme ! ). 

Alors, quand il s’agit d’un bassin versant de 100 km de côté ça donne ça... mais imaginez un truc qui est des dizaines de milliers de fois plus grand, du genre la Terre !



<img src="/images/bodensee/cygne-obersee.JPG" width="100%" title="Le cygne et le paddleur. Lac supérieur avec vue sur les Alpes (fév. 2022)">



[^vonlanthen]:  P. Vonlanthen et al. [Eutrophication causes speciation reversal in whitefish adaptive radiations](https://doi.org/10.1038/nature10824). 2012.

[^schindler]:  D.W. Schindler. [The dilemma of controlling cultural eutrophication of lakes](https://doi.org/10.1098/rspb.2012.1032). 2012.

[^stich]:  H.B. Stich & A. Brinker. [Oligotrophication outweighs effects of global warming in a large, deep, stratified lake ecosystem](https://doi.org/10.1111/j.1365-2486.2009.02005.x). 2010.

[^rhymer]: Review on introgressive hybridization, with focus on introduced species: J.M. Rhymer and D. Simberloff. [Extinction by hybridization and introgression](https://doi.org/10.1146/annurev.ecolsys.27.1.83). 1996. 

[^rosenzweig]: M.L. Rosenzweig. [Loss of speciation rate will impoverish future diversity](https://doi.org/10.1073/pnas.101092798). 2001.

[^barnett]: A. Barnett & B.E. Beisner. [Zooplankton biodiversity and lake trophic state: explanations invoking resource abundance and distribution.](https://doi.org/10.1890/06-1056.1) 2007.

[^wang]: H. Wang et al. [Eutrophication causes invertebrate biodiversity loss and decreases cross-taxon  congruence across anthropogenically-disturbed lakes](https://doi.org/10.1016/j.envint.2021.106494). 2021.

[^lake-oxygen]: S. Fane et al. [Widespread deoxygenation of temperate lakes](https://doi.org/10.1038/s41586-021-03550-y). 2019.

[^baer]: **J.Baer et al.** Managing Upper Lake Constance fishery in a multi-sector policy landscape: Beneficiary and victim of a century of anthropogenic trophic change. Chap. 3 de [Inter-sectoral governance of inland fisheries](http://toobigtoignore.net/research-highlights-1/e-book-inter-sectoral-governance-of-inland-fisheries/). 2017.





