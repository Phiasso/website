---
layout: post
title:  "Les meufs vénères"
date:   2020-04-12 22:15:08 +0200
update: 2021-01-11 16:15:08 +0200
categories: post
permalink: "/les-meufs-veneres/"
lang-ref: mfvnr
lang: fr
---

Update: 9 months later, Jan 12, 2021

# Ou de la légitimité de la colère des femmes

---

*Notes développées suite à la lecture de [Sorcières](https://www.editions-zones.fr/lyber?sorcieres) de Mona Chollet, [en libre accès sur le site de l'éditeur](https://www.editions-zones.fr/lyber?sorcieres).*  
*Y sont ajoutées des idées d'une lecture ancienne du [Deuxième Sexe](https://fr.wikipedia.org/wiki/Le_Deuxi%C3%A8me_Sexe) de Simone de Beauvoir, complétées par quelques autres références, citées directement dans le texte.*

---

<div class="img-with-text" style="float:right">
<img src="/images/sorciere.jpg" alt="Baba Yaga" width="200"
	title="Baba Yaga"
	/>
	<p><font size="2">Baba Yaga. <br> 
		<i>Illustration <a href="http://www.fairyroom.ru/?p=12443">Boris Zabirokhine</a></i>.</font></p>
</div>

Il s'agit ici d'un petit texte visant à contextualiser les luttes féministes actuelles dans les pays occidentaux (plus particulièrement en Europe et en France pour l'histoire récente, sauf mention du contraire). Les contextualiser à travers l'Histoire (sans remonter au-delà du Moyen Âge), dans la réalité qu'ont vécu les femmes d'un point de vue social, politique, individuel.. pour en arriver à comprendre ce que nous, les femmes en particulier, vivons aujourd'hui.  

La critique vise non pas des individus (même si des comportements individuels peuvent être et sont problématiques), mais les institutions existantes qui légitiment et perpétuent une domination (patriarcale, sexiste, machiste...) sur les femmes. Et s'il faut encore le rappeler : le féminisme n'est pas une guerre contre les hommes, mais un combat *pour* leurs privilèges et *contre* leur domination, pour une *égalité dans la liberté et non dans l'oppression*.  

### Table des contents

* [ Une certaine domination ou une domination certaine ](#domination)  

  * [ Dans les sphères politique et sociale ](#piso)  
  * [ Dans les sphères privée et familiale ](#prifa)  
  * [ À l'interface ](#interface)  
  * [ Représentations](#representations)
  * [Et de l'interiorisation](#interiorisation)  
  <br>

* [ Des volontés d'émancipation ](#emancipation)  

	* [ De l'importance de donner la parole ](#parole)
	* [ Un mouvement social comme les autres ? ](#mouvsoc)
	* [ L'antiféminisme malgré tout ](#antifem)
	* [ Et de la révolte ? ](#revolte)
	* [ Pourquoi on peut s'en réjouir &#9786;](#content)  
<br>

* [ *Références supplémentaires* ](#refs)

---

<a name="domination"></a>
## Une certaine domination ou une domination certaine 

Dans les paragraphes suivants, je vais beaucoup parler d'un certain Système. Système, ce n'est pas un individu, c'est un ensemble composé d'individus qui interagissent entre eux. En l'occurrence ce Système est plutôt culturel et social et, dans sa forme actuelle, est le fruit de siècles, voire de millénaires d'histoire, de *traditions* comme on aime dire, qui pèsent sacrément sur nos esprits, donc sur la façon dont on vit, dont on conçoit la vie, dont on se conçoit soi-même et dont on conçoit les autres. 

En ce qui concerne les femmes, Système n'aura pas été particulièrement clément. Il a même été sacrément oppressif, intrusif, répressif, censeur, dominateur, exploiteur... plutôt méchant, quoi. C'est ce rapport de Système aux femmes que j'essaierai d'illustrer dans les paragraphes suivants: au fil du temps et dans tous les domaines de la vie des femmes. Et Système est sacrément puissant et difficile à changer, parce qu'il est dans, et a façonné nos têtes - nos têtes de femmes. 

<a name="piso"></a>
### Dans les sphères politique et sociale

#### Au Moyen Âge

*[L'Histoire de la femme aux Vᵉ-XVᵉ siècles](https://fr.wikipedia.org/wiki/Femmes_au_Moyen_%C3%82ge) est assez floue, mais quelques études en dressent le contour suivant :*  

Si la plupart des femmes au Moyen Âge sont mariées, sous la tutelle de leur mari (sinon sous la tutelle d'une famille), il semblerait qu'elles jouissent quand même de certaines formes d'autonomie. Elles sont (peu, mais quand même) représentées dans la vie politique, militaire (les chevaleresses), religieuse (les abbesses dans les couvents avaient un pouvoir égal à celui des abbés), scientifique (dans les couvents encore), artistique (compositrices, écrivaines, trobairitz, trouveresses..), etc. Dans la sphère familiale, elles sont certes souvent cantonnées aux travaux ménagers et à la maison, mais il arrive de tomber sur des éleveuses, artisanes, sages-femmes, médecins, guérisseuses, brasseuses, métallurgistes...     
En dehors du cadre de la famille traditionnelle, existent même des communautés qu'on appelle **béguines**, composées surtout de femmes veuves vivant dans des maisons individuelles et cultivant ensemble des potagers ou jardins de plantes médicinales. Aline Kiener les met en scène dans son roman [*La nuit des béguines*](https://www.lianalevi.fr/catalogue/la-nuit-des-beguines/) 🧙‍♀️. 

La situation d'une femme dépend cependant beaucoup de son statut social. Alors que la vie d'une paysanne n'a que peu d'importance, ce ne sera pas le cas des nobles. Même leurs virginités avaient des valeurs différentes fonction de leur statut. 

>Par nature, la fille du chevalier est plus pure que celle du bas peuple. La paysanne, la bergère sont violées au détour d'un chemin par le clerc ou le noble sans que nul ne s'en offusque. Dans la littérature courtoise, le motif de viol de la pastourelle par le chevalier est un thème récurrent.
>
>**Sophie Cassagnes-Brouquet**, *La vie des femmes au Moyen Âge*

- Pour aller plus loin sur ce sujet particulier : une [Histoire du viol](https://www.seuil.com/ouvrage/histoire-du-viol-georges-vigarello/9782020403641) de Georges Vigarello.

À la fin du Moyen Âge, l'autonomie de la femme devient sujet de flippe (oui, *à la fin du Moyen Âge*... voire début de la Renaissance). Les béguines se font dézinguer -- brûler souvent -- ça sent le début des **chasses aux sorcières**.  
On assiste à la naissance du mythe de la sorcière, dont la propagation coïncide avec celle de l'imprimerie (1454) qui a entre autres permis une médiatisation importante (30 000 exemplaires) d'un charmant bouquin *[Le Marteau de la sorcière (Malleus maleficarum)](https://fr.wikipedia.org/wiki/Malleus_Maleficarum)* (1487) écrit par deux inquisiteurs alsacien et bâlois. La chasse aux sorcières, qui dans l'imaginaire collectif est un événement anecdotique si ce n'est rigolo, comme le souligne Mona Chollet, est de fait, un crime de masse perpétué contre une moitié de la population. 

#### À la Renaissance 

[Aux XVᵉ-XVIᵉ siècles](https://fr.wikipedia.org/wiki/Histoire_de_la_femme#Renaissance), le droit des femmes recule : elles deviennent mineures et se font exclure de la vie politique et publique. Elles se font peu à peu virer de tous les corps de métiers dans lesquels elles étaient représentées (trop de concurrence, notamment dans l'artisanat) et sont reléguées aux travaux domestiques exclusivement.  

La chasse aux sorcières débute donc surtout à cette période, le XVᵉ siècle, et est perpétrée jusqu'à la fin du XVIIIᵉ avec un apogée des massacres vers 1560. Outre les meurtres, on invente tout un arsenal d'instruments de torture comme le [scold's bridle](http://www.historyofmasks.net/famous-masks/scolds-bridle/) pour museler les femmes accusées de sorcellerie.. ou plutôt celles qui parlent trop.
Pour Françoise d'Eaubonne dans [*Sexocide des sorcières*](https://booksofdante.wordpress.com/2013/07/13/le-sexocide-des-sorcieres/), qui insiste sur la dimension féminicide des chasses, il s'agit du "déchaine[ment d']un massacre par un raisonnement digne d'un aliéné" sans autre raison que la volonté d'éliminer les femmes "trop" libres.
Ce mythe a créé des vocations dans ce qui a été appelé *la démonologie*. Le diable est, en effet, l'autorité des sorcières. Mais le diable n'était qu'une métaphore de leur autonomie. Car pour tout bon démonologue, une femme autonome est inconcevable : même les plus libres doivent bien être sous l'autorité d'une figure masculine, celle du diable 👹.

* Sur la tran­si­tion entre le féo­da­lisme et le capi­ta­lisme, et notamment la répartition des femmes dans les métiers au Moyen Âge lire [Silvia Federici, Caliban et la sorcière](https://entremonde.net/caliban-et-la-sorciere).

#### Au siècle des Lumières 

[Au XVIIᵉ](https://fr.wikipedia.org/wiki/Condition_f%C3%A9minine_au_si%C3%A8cle_des_Lumi%C3%A8res), les bourgeoises peuvent certes tenir des salons, et il existe quelques exceptions de femmes dans les sciences, les arts ou la politique (issues de classes privilégiées). Mais dans les milieux populaires, celles qui exercent des métiers sont servantes ou marchandes. Et c'est tout (j'exagère à peine).  
D'ailleurs sur les femmes et la science à cette époque on peut lire :

> « La recherche des vérités abstraites et spéculatives, des principes, des axiomes dans les sciences, tout ce qui tend à généraliser les idées n'est point du ressort des femmes, leurs études doivent se rapporter toutes à la pratique ; c'est à elles de faire l'application des principes que l'homme a trouvés, et c'est à elles de faire les observations qui mènent l'homme à l'établissement des principes »  
>
> 👩‍🔬 ?
>
> **Rousseau**, *La Nouvelle Héloise*  

À cette époque, il y a certes encore des sages-femmes. Mais avec l'arrivée de chirurgiens-accoucheurs, la pratique de la chirurgie devient interdite aux femmes. Les femmes ne peuvent donc même plus aider d'autres femmes à donner naissance.

#### Sous la Révolution française

Sous la révolution, on peut résumer la situation ainsi : si elles ouvrent leur gueule (et ça arrive), les femmes sont bâillonnées, voire exécutées. On autorise le divorce, mais on leur interdit de se réunir entre elles. Malgré des initiatives comme la [Déclaration des droits de la femme et de la citoyenne](https://fr.wikipedia.org/wiki/D%C3%A9claration_des_droits_de_la_femme_et_de_la_citoyenne) d’Olympe de Gouges en 1791, la révolution n'a que peu fait évoluer les choses dans le sens d'une émancipation pour les femmes estime Simone de Beauvoir.

#### Pendant la Révolution industrielle

Plus tard, avec la révolution industrielle, une nouvelle figure émerge : celle de l'ouvrière 👩‍🏭. On l'envoie vers les manufactures -- textiles en premier lieu -- "pour son agilité", "sa patience" et "sa docilité". Comme elle perçoit un salaire -- un premier pas vers l'indépendance -- les syndicalistes s'y opposent, pour raison de concurrence *déloyale* et *désorganisation du foyer*. Proudhon (fameux anarchiste) et ses fidèles sont d'ailleurs attachés à une *répartition sexuée des rôles* dans la société.   
Les bourgeoises ne réclament rien, les ouvrières se mobilisent. Lors de la révolution de 1848, deux revendications sont portées : le droit au travail ainsi que le droit de vote, mais ne sont pas entendues.  

* Jeanne-Marie Wailly, [Les différentes phases du travail des femmes dans l'industrie ](https://www.cairn.info/revue-innovations-2004-2-page-131.html)  

#### au XXᵉ siècle

Pendant les deux guerres mondiales on se rend compte que, merde, on a besoin de femmes dans les usines, vu que tous les hommes ont été envoyés, abattus et/ou estropiés au front. À la fin de la guerre, elles (certaines qu'on aura au passage tondu si elles ont été infidèles avec l'ennemi) sont renvoyées des usines chez elles pour "repeupler le pays". En période de crise économique on évince donc à nouveau les femmes du monde du travail pour que les hommes ne soient pas soumis à une concurrence trop rude. Cette politique est appuyée par des lois discriminatoires, qui causent une baisse des salaires des femmes, et seraient à l'origine de l'inégalité salariale.  
Ah, que je n'oublie pas : en 1944 les Françaises obtiennent quand même le droit de vote ! -- youpi 🥳.  

Deuxième moitié du 20ᵉ, en 1974, faisons un saut aux États-Unis : il y est impossible pour une femme de travailler dans les secteurs où les femmes ne sont pas représentées. "Sauvez un emploi, tuez une femme" voit-on affiché dans une usine de colorants (American Cyanamid) où travaille, et milite pour travailler, Betty Riggs (au passage, petite anecdote : elle se fait casser la gueule et brûler sa voiture par son mari).

Bref, durant tous ces siècles, le statut de la femme a oscillé entre peu et pas d'autonomie d'un point de vue juridique et d'accès aux droits ;  les femmes se sont fait évincer de corps de métiers et/ou reléguées à des postes aux salaires très bas ; la femme a été soustraite à la vie politique et publique. Par les hommes. Les femmes n'ont donc que très peu laissé de traces dans l'Histoire. Elles ne l'ont d'ailleurs pas non-plus écrite.. et même si elles l'enseignent de plus en plus aujourd'hui, [parfois de façon militante](https://www.youtube.com/watch?v=3rpge3SriTo), il reste un sacré filtre à outrepasser.

#### Et aujourd'hui ?

> Aujourd’hui, en France, non seulement nombre de travailleuses sont à temps partiel (un tiers des femmes, contre 8 % des hommes) et n’ont donc pas d’indépendance financière – c’est-à-dire pas d’indépendance tout court –, mais elles sont cantonnées dans des professions liées à l’éducation, au soin des enfants et des personnes âgées, ou dans des fonctions d’assistance : « Près de la moitié des femmes (47 %) se concentre toujours dans une dizaine de métiers comme infirmière (87,7 % de femmes), aide à domicile ou assistante maternelle (97,7 %), agent d’entretien, secrétaire ou enseignante. ["Répartition femmes/hommes par métiers : l’étude de la Dares", Secrétariat d’État en charge de l’égalité entre les femmes et les hommes, 13 décembre 2013]»  
> 
> *[Sorcières]* 

N'en déplaise à certain·e·s, c'est une situation qui, évidemment, n'a rien d'essentialiste : il n'y a de qualités ni de compétences qui seraient spécifiquement et *par essence* féminines ou masculines. Simplement des qualités et des compétences rendues féminines par des constructions sociales et aujourd'hui imposées aux femmes.  
Isabelle Collet en parle très bien dans [un entretien dans l'émission *les couilles sur la table*](https://www.binge.audio/des-ordis-des-souris-et-des-hommes/), avec l'exemple des métiers du numérique 👩‍💻.

Si tant est qu'on accède aux mêmes postes, dans les mêmes entreprises que les hommes (peut-être pas les mêmes salaires quand même), alors on est confrontées au sexisme ordinaire qui court sur les lieux de travail (pas exclusivement, évidemment), dont on peut écouter quelques exemples dans ce *Podcast à soi*, [Sexisme ordinaire en milieu tempéré](https://www.arteradio.com/son/61659100/sexisme_ordinaire_en_milieu_tempere_1).

Outre le sexisme ordinaire que l'on rencontre à tout coin de rue, l'autre fléau qui ronge notre Système est une [culture du viol](https://fr.wikipedia.org/wiki/Culture_du_viol), c’est-à-dire la tolérance sociale du viol. Elle s'illustre par des pirouettes malhonnêtes rendant la victime responsable : "elle l'a mérité", "elle l'a voulu", "elle a kiffé", une négation du non-consentement.. ; par des tentatives de distinction entre vrai et faux viol ; par des obstacles à la dénonciation...  La culture du viol s'illustre également brillamment dans les sphères politiques et culturelles : en 2012 la figure la plus charismatique du PS était un violeur en bande organisée, en 2020 on sacre aux Césars un violeur qui a fui la justice et les pédophiles sous couvert de génie littéraire ont toujours pignon-sur-rue.

<a name="prifa"></a>
### Dans les sphères privée et familiale

#### À travers les âges...

La subordination de la femme à un membre de sa famille de sexe masculin à travers les âges n'est plus vraiment à démontrer. La femme a toujours été soit propriété, soit mineure, ou sous l'autorité ou la tutelle d'un mari, d'un père ou d'un frère. *Voir le 2ᵉ Sexe Tome I, pour l'évolution de cette situation au cours de l'Histoire.*  
De toute façon le mari a le droit de faire ce qu'il veut du corps de sa femme, et le viol de la femme par son mari n'existe pas, ne peut pas exister (c'est comme les violences policières dans un État de droit).

Un autre édifiant exemple des Lumières : 
> « Ainsi toute l'éducation des femmes doit être relative aux hommes. Leur plaire, leur être utiles, se faire aimer et honorer d'eux, les élever jeunes, les soigner grands, les conseiller, les consoler, leur rendre la vie agréable et douce, voilà les devoirs des femmes dans tous les temps et ce qu'on doit leur apprendre dès leur enfance. »   
> Rousseau, *Emile ou de l'éducation*

Au tout début du XIXᵉ siècle, sous Napoléon, le code civil (1804) prévoit que : « le mari doit protection à la femme, la femme doit obéissance à son mari » et rétablit l'autorité du père.  
Ce n'est qu'en 1970 que l’autorité parentale remplace la puissance paternelle.

#### Actuellement

Aujourd'hui, les violences conjugales sévissent dans l'intimité des foyers. Ce sont les vestiges de cette idée de *propriété* que l'homme aurait sur le corps de la femme. Et ça arrive souvent parce que les femmes quittent ou ont l'intention de quitter leur partenaire : "l'État n'organise plus des exécutions publiques de prétendues sorcières, mais la peine de mort pour les femmes qui veulent être libres s'est en quelque sorte privatisée" (*Sorcières*).  

On peut également rentrer dans des éléments un peu plus anecdotiques mais révélateurs des mœurs et mentalités des siècles passés. Citons la répartition des tâches ménagères qui est toujours très inégale. Notons aussi, l'écart d'âge qu'on voit fréquemment au sein des couples hétérosexuels : 
> « L'écart d'age dans les couples actuels représente [pour le sociologue Eric Macé](https://www.lesinrocks.com/2017/06/17/actualite/actualite/les-hommes-sont-ils-eux-aussi-victimes-dage-shaming/) la trace de l'époque "où la définition sociale des femmes se faisait par la conjugalité reproductive", où l'homme en vieillissant "augmentait son pouvoir économique et social" tandis que la femme "perdait son capital corporel: la beauté et sa fécondité". »  

D'aucuns interprètent la recherche de jeunes compagnes par des hommes d'âge plus "mûr" (qui auront, souvent au préalable, quitté une femme du même âge qu'eux) comme la volonté de fuir sa propre vieillesse, mais surtout la recherche de domination qui est plus facile lorsque la compagne est jeune, peu expérimentée, et avec peu de recul sur ses relations. 👴🏻👧  
D'ailleurs, l'expérience est considérée comme une qualité rassurante chez les hommes, un défaut inquiétant chez les femmes.

* Pour aller plus loin : [Histoire des normes sexuelles : l’emprise de l’âge et du genre](https://journals.openedition.org/clio/12823)

* Pour une vision sociale et plus psychanalytique du couple hétérosexuel : Liv Strömquist, [Les sentiments du prince Charles](https://fr.wikipedia.org/wiki/Les_Sentiments_du_prince_Charles).

<a name="interface"></a>
### À l'interface

#### Maternité

**L'accouchement**  
La maternité a longtemps été gérée entre femmes par les femmes, jusqu'à sa médicalisation vers le XVIIIᵉ siècle, où les sages-femmes ont été reléguées au statut d'infirmière assistant le docteur. Le coup de grâce a été donné au moment de l'invention du forceps (au XVIᵉ), classé comme instrument chirurgical et donc... interdit aux femmes (car elles n'avaient pas le droit de pratiquer la chirurgie). L'invention du spéculum au XIXᵉ par un médecin de l'Alabama (lui il pratiquait des expériences chirurgicales sur des esclaves sans anesthésie), finit d'entériner cette affaire. L'accouchement était devenu affaire (très sanglante) d'hommes. 
Aujourd'hui, l'accouchement se pratique avec une femme allongée sur le dos (la *pire* position pour que le bébé puisse sortir, paraît-il...), les jambes écartées pour que le médecin (et quiconque passerait par là) puisse jeter un œil sur son intimité exposée.    

Car la médicalisation n'est pas toujours synonyme de progrès pour celles qui sont en demande de soins. Au XIXᵉ siècle sévissait la fièvre puerpérale avec une mortalité énorme chez les femmes qui accouchaient à l'hôpital (jusqu'à une femme sur quatre qui avait accouché à la maternité parisienne en hiver 1866). Et ce jusqu'au moment où un médecin comprit que ce n'était pas une bonne pratique que de disséquer un cadavre, puis aller faire accoucher une future maman sans se laver les mains (ses confrères en étaient d'ailleurs bien offusqués !).

>« L’effondrement de la mortalité en couches entre 1945 et 1950 est le résultat de l’amélioration des conditions de vie, de l’hygiène et des progrès de la médecine en général, bien plus que de l’interventionnisme obstétrical au moment de l’accouchement. »   
>
> **Marie-Hélène Lahaye**, *Accouchement : les femmes méritent mieux*  
> *[Sorcières]*


**Être mère**  
Au XIXᵉ siècle, des théories fallacieuses comme celle de la « conservation d’énergie », appliquées aux femmes enceintes tentaient de justifier l'exclusion de celles-ci de l'éducation supérieure : 

> « Les organes et les fonctions du corps humain étaient censés lutter pour s’approprier la quantité limitée d’énergie qui y circule. Dès lors, les femmes, dont l’existence avait pour but suprême la reproduction, devaient "conserver leur énergie en elles, autour de l’utérus", expliquent Barbara Ehrenreich et Deirdre English. Enceintes, elles devaient rester allongées et éviter toute autre activité, en particulier intellectuelle : "Les médecins et les pédagogues ont rapidement conclu que l’éducation supérieure pouvait être dangereuse pour la santé des femmes. Une croissance cérébrale trop soutenue, avertissaient-ils, atrophierait l’utérus. Le développement du système reproducteur ne permettait tout simplement pas le développement de l’intelligence." »  
> 
> **Barbara EHRENREICH** et **Deirdre ENGLISH**, *Fragiles ou contagieuses. Le pouvoir médical et le corps des femmes*.  
> *[Sorcières]*

C'est ainsi que la notion de sacrifice maternel a été mise au goût du jour. 🤱  
Aujourd'hui, sacrifier sa carrière, sa profession, son être et soi dans la maternité pour se dévouer totalement à ses enfants est non-seulement fréquent, mais glorifié. Lorsqu'on s'y refuse on est accusée de mégère. Lorsque c'est la maternité qu'on ne souhaite pas, on est face à une incompréhension, des "t'inquiète, t'en auras envie quand tu trouveras la bonne personne", voire des injonctions à se dépasser, car faire des enfants permet de se transcender en tant qu'individu. Et alors quand, le fait accompli, il s'agit d'émettre des doutes ou des regrets sur sa maternité, c'est carrément tabou. 

* La question du choix réel d'avoir ou non un enfant est longuement discutée dans *Sorcière* : [Le désir de la stérilité. Pas d'enfant, une possibilité.](https://www.editions-zones.fr/lyber?sorcieres#chap-003).

Et c'est sans aborder les grossesses forcées non désirées parfois issues de viols, qui peuvent aboutir à des situations de désespoir total pour les femmes qui en sont victimes. Un témoignage parmi d'autres :

> Un collectif féministe français a recueilli en 2006 le témoignage d’une anonyme, vraisemblablement âgée, qui racontait avoir, par deux fois, accouché seule et étouffé le bébé. Elle s’était mariée à dix-huit ans et, à vingt et un ans, elle avait déjà trois enfants, avec lesquels elle était enfermée à la maison. « J’avais l’impression d’être un tiroir : toc, on met un enfant et, quand c’est vide, on en remet un autre, et voilà. » Lorsqu’elle essaie de se soustraire aux rapports sexuels, son mari la bat. « Je n’avais pas à avoir de désirs, j’avais tout ce que je pouvais désirer, paraît-il : je mangeais tous les jours, j’avais des enfants qui allaient à l’école. Il ne voulait pas savoir si j’avais d’autres espérances, c’était le moindre de ses soucis. » Elle essaie d’avorter par tous les moyens et y parvient neuf fois, mais cela ne marche pas toujours. « C’est une situation inhumaine mais, au moment où tu le fais, c’est ta seule solution. »  
> 
> *Sorcières*

* Sur les questions autour de la maternité, quelques témoignages dans ce *Podcast à soi* : [Le gynécologue et la sorcière](https://www.arteradio.com/son/61659783/le_gynecologue_et_la_sorciere_6).

#### La maitrise de son corps

Dans les paragraphes précédents, c'est finalement de l'emprise des hommes sur le corps des femmes qu'il est question : du père sur la fille, du mari sur sa femme, du médecin sur sa patiente, en fait généralement de la société sur ses femmes. De ce fameux Système. 

Avoir la possibilité de maîtriser son corps, de choisir indépendamment de la volonté d'un mari ou d'un violeur -- c'est la question centrale de la contraception et de l'IVG.  

Une guerre sans merci était menée contre la contraception et l'IVG depuis l'époque de la chasse aux sorcières, pendant et avant laquelle l'IVG se pratiquait plus ou moins clandestinement par des guérisseuses ou sages-femmes.  
Au XXᵉ, autour des débats sur la contraception -- entre hommes -- les hommes avaient peur d'une "grève des ventres". En 1920, la contraception est assimilée à l'avortement et donc à un crime. Ce n'est qu'en 1967 qu'elle est autorisée en France. Et l'IVG en 1975 avec la loi Veil.  
À noter, que dès le début des années 60, refusant leur légalisation, la France encourageait pourtant la contraception et l'IVG ainsi que des stérilisations forcées dans les départements d'Outre-Mer... Deux poids deux mesures, donc.

Et s'il leur faut faire une place dans le paysage actuel, les mouvements "pro-life", c’est-à-dire anti-avortement, n'ont qu'un but : celui d'empêcher les femmes d'avoir la possibilité de maîtriser leur corps. Sans se soucier de leur bien-être ni de leur volonté : "Le natalisme est affaire de pouvoir, et non d'amour de l'humanité",  *Sorcières*.

#### La médecine

> La médecine semble bien avoir été la scène centrale sur laquelle s’est jouée la guerre de la science moderne contre les femmes.  
> La médecine telle que nous la connaissons s’est construite sur leur élimination physique : les chasses aux sorcières ont visé d’abord les guérisseuses[...]. S’appuyant sur l’expérience, celles-ci étaient bien plus compétentes que les médecins officiels, dont beaucoup étaient de piteux Diafoirus, mais qui profitèrent néanmoins de l’élimination de cette concurrence « déloyale » tout en s’appropriant nombre de leurs découvertes. Dès le XIIIᵉ siècle, cependant – soit bien avant le début des chasses aux sorcières –, avec l’apparition d’écoles de médecine dans les universités européennes, la profession médicale avait été interdite aux femmes. En 1322, Jacqueline Félicie de Almania, une noble florentine qui s’était installée à Paris, fut traînée devant les tribunaux par la faculté de la ville pour exercice illégal de la médecine. Six témoins affirmèrent qu’elle les avait guéris, et l’un d’eux déclara qu’elle « en savait plus dans l’art de la chirurgie et de la médecine que les plus grands médecins ou chirurgiens de Paris », mais cela ne fit qu’aggraver son cas, puisque, en tant que femme, elle n’était tout simplement pas censée exercer.  
> Le destin du recueil dit *Trotula*, consacré aux maladies gynécologiques et baptisé ainsi en référence à Trota, une fameuse soignante de Salerne, montre bien le processus d’effacement des femmes non seulement dans la pratique, mais aussi dans la constitution de la littérature médicale. Assemblé à la fin du XIIᵉ siècle, le *Trotula* connaît diverses tribulations et finit par atterrir en 1566 entre les mains d’un éditeur allemand qui l’intègre à un ensemble plus vaste, le *Gynaeciorum libri*. Mettant en doute l’identité de Trota, il l’attribue à un médecin nommé Eros. « De la sorte, la liste des auteurs grecs, latins et arabes rassemblés dans le Gynaeciorum accuse une remarquable homogénéité : ce sont tous des hommes discourant sur le corps féminin et s’affichant comme les vrais détenteurs du savoir gynécologique », conclut Dominique Brancher. Aux États-Unis, où la profession médicale est encore plus masculine qu’en Europe, l’éviction des femmes s’est produite plus tardivement, au XIXᵉ siècle. Le coup de force de l’homme (en) blanc issu de la classe moyenne suscita une résistance acharnée, notamment à travers le Mouvement populaire pour la santé, mais il fut finalement victorieux.  
> 
> **Dominique Brancher**, *Équivoques de la pudeur. Fabrique d’une passion à la Renaissance*.  
> *Sorcières*

On aura donc évincé les femmes du corps médical, et maintenant il s'agit de les soumettre (et pas uniquement elles) au corps médical :

> Comme l’écrit Marie-Hélène Lahaye, brandir le spectre de la mort « est la meilleure arme pour dissuader les femmes d’aspirer au respect de leur corps et pour maintenir leur soumission au pouvoir médical ». À en croire Martin Winckler, c’est aussi la meilleure arme pour dissuader les étudiants en médecine de poser trop de questions sur les pratiques qu’on leur enseigne, en les terrorisant : « Si tu n’apprends pas les bons gestes, et si tu ne les fais pas comme on te les apprend, les patients mourront. » Souvent, la menace est très exagérée – en particulier s’agissant des femmes enceintes, qui ne sont pas malades. Mais soit : parfois, elle est bien réelle. Face à un médecin, on est toujours en position de faiblesse : parce qu’on souffre d’une affection plus ou moins grave, et éventuellement mortelle ; parce qu’il détient un savoir qu’on n’a pas et que si quelqu’un a le pouvoir de nous sauver, c’est lui ; parce qu’on est couché et qu’il est debout, comme disait Desproges. Mais cette situation de vulnérabilité devrait plaider pour qu’il fasse preuve d’un minimum d’égards, pas pour que le malade la boucle. Elle a d’ailleurs tendance à exacerber toutes les émotions : elle rend la maltraitance d’autant plus blessante, et elle suscite une gratitude éternelle quand on a affaire à un soignant qui se comporte avec empathie et délicatesse.  
> 
> **Marie-Hélène LAHAYE**, *Accouchement : les femmes méritent mieux*  
> **M. Winckler**, *Les brutes en blanc*.  
> *Sorcières*

Il y a aussi ce biais médical qui fait que les symptômes des femmes sont minimisés, moins pris au sérieux que ceux des hommes : 
>"À symptôme égal, une patiente qui se plaint d’oppression dans la poitrine se verra prescrire des anxiolytiques, alors qu’un homme sera orienté vers un cardiologue."  
>
> **Marie Campistron**, *« “Les stéréotypes de genre jouent sur l’attitude des médecins comme des patients” »* 
> *Sorcières*


#### Et au-delà

Cette critique d'une froideur, d'un détachement médical (soi-disant nécessaire à l'exercice des soins) d'objectification du patient, est aussi celle d'une instrumentalisation de la rationalité pour en justifier. Cette critique peut s'inscrire dans celle, plus globale :

> Une « réaction en chaîne » [...] : la lente extension à toute la planète, à partir de l’Occident de la Renaissance, d’une logique marchande, froidement calculatrice, présentée à tort comme un sommet de rationalité.  
> 
> **Jean-François BILLETER**, *Chine trois fois muette*  
> *Sorcières*

Un monde où tout est mécanique, les animaux sont des automates, où le raisonnement est une simple suite d'opérations mathématiques, où tout est clair et objectif, et le corps est séparé de l'âme. Des concepts, en somme qui feront  d'après [Silvia Federici](https://entremonde.net/caliban-et-la-sorciere) l'«instrument adéquat à la régularité et aux automatismes requis par la discipline capitaliste».  

En réaction à cette science exclusivement masculine et construite autour du masculin, à cette vision du monde froide, distanciée, d'une nature morte utilisée uniquement à des fins d'extraction de ressources, a surgi le courant [écofeministe](https://www.cairn.info/revue-multitudes-2017-2-page-29.htm), qui combat la domination de la nature en "symbiose" avec le combat contre la domination de la femme. 

<a name="representations"></a>
### Représentations

#### De l'importance des symboles

> Bien que nous vivions dans des sociétés largement sécularisées, et bien que nombre de femmes et d’hommes ne croient plus en Dieu, explique l’écrivaine écoféministe Carol P. Christ, les religions patriarcales ont façonné notre culture, nos valeurs et nos représentations, et nous restons imprégnés d’un modèle d’autorité masculin qui en est directement issu : « La raison de la persistance effective des symboles religieux réside dans le fait que l’esprit a horreur du vide. Les systèmes symboliques ne peuvent pas simplement être rejetés ; ils doivent être remplacés. »  
> 
> **Carol P. Christ**, *Pourquoi les femmes ont besoin de la déesse : réflexions phénoménologiques, psychologiques et politiques*  
> *Sorcières*

Au-delà des symboles religieux ou spirituels, ce sont simplement les symboles qui peuplent notre imaginaire, et qui sont largement influencés par des représentations héritées de l'Histoire et actuelles, notamment de la femme, qui façonnent notre vision qu'on a de celle-ci. 

#### L'inacceptable laideur féminine

>"Toute femme devrait être accablée de honte à la pensée qu'elle est femme." (Clément d'Alexandrie). Déjà chez Aristote, et bien avant les Pères de l'Église, la femme est matière sans qualité aucune, la qualité restant à l'évidence le propre de l'homme. Tel est le paradoxe du "beau sexe" : source du péché, sa plaisante apparence ne peut que dissimuler un être répugnant. Plus tard, sa beauté enfin reconnue, la femme se voit sommée de s'épanouir dans le mariage et dans la maternité. Haro donc sur les célibataires, les bas-bleus, les féministes, les inverties et autres déviantes de la société, qui ne sont que disgrâce et souvent même monstruosité ! De nos jours, enfin, disposant d'un vaste attirail cosmétique, la voici inexorablement soumise à la tyrannie de la séduction permanente. Insupportable, inexcusable, véritable aberration sociale, la laideur féminine révèle crûment négligence, manque de volonté, pire, secrète pathologie. S'appuyant tout à la fois sur l'histoire, l'anthropologie, la littérature et la peinture, Claudine Sagaert, par cette contribution essentielle à l'histoire des genres, nous permet de mieux comprendre dans quel carcan le corps de la femme a été enfermé durant des siècles, carcan dont elle doit, aujourd'hui comme hier, toujours se libérer...
>
>**Claudine Sagaert**
>
>Résumé de l'*Histoire de la laideur féminine*

Si ce ne sont pas toutes les femmes qui sont laides, ce sont donc les célibataires, les rebelles, celles qui ouvrent un peu trop leur gueule : "Toutes les insoumises sont laides" résume David Lebreton dans la préface à l'*Histoire de la laideur féminine*. Mais aussi docile soit-elle, toute femme vieille est laide : le corps d'une vieille femme inspire dégoût et effroi.

* Un article là-dessus : Juliette Rennes, [Vieillir au féminin](https://www.monde-diplomatique.fr/2016/12/RENNES/56899) 👵

Cette "laideur naturelle" ne peut être atténuée que par le moyen de mille artifices, en tendant vers des canons de beauté totalement déshumanisants : tantôt flasque, tantôt blanche comme un cadavre (au 19ᵉ, on leur faisait bouffer de l'arsenic pour avoir un teint bien livide -- exemple de l'[*Arsenic Complexion Wafer*](http://thequackdoctor.com/index.php/dr-mackenzies-improved-harmless-arsenic-complexion-wafers/)), tantôt squelettique, prude ou vulgaire, dévoilant un max de peau avec l'interdiction formelle d'y laisser un seul poil.  
Outre les complexes, il n'est pas étonnant de voir toute une panoplie de troubles se développer, surtout chez les adolescentes. Ils leur resteront une grande partie de leur vie, faute de pouvoir se conformer à un idéal irréaliste qui ne fait que casser et mutiler le corps dans la haine des corps.

#### Les femmes vues par les hommes, les hommes vus par les hommes

Les représentations de la femme sont souvent réalisées par des hommes. Et on parle aujourd'hui dans certains cas de [*male gaze*](https://antisexisme.net/2014/01/12/lobjectivation-sexuelle-des-femmes-un-puissant-outil-du-patriarcat-le-regard-masculin/), le regard masculin cause d'objectivation sexuelle de la femme. 

* *Les couilles sur la table* : [male gaze, ce que voient les hommes](https://www.binge.audio/male-gaze-ce-que-voient-les-hommes/) vs [female gaze, ce que vivent les femmes](https://www.binge.audio/female-gaze-ce-que-vivent-les-femmes/) 

Les femmes sont la chair, les hommes sont l'esprit. "Les hommes n'ont pas de corps" écrit **Virginie Despentes** dans *King Kong théorie*. Et en effet, on voit qu'ils peuvent, en comparaison aux femmes, bien souvent se soustraire de tout critique relative à leurs caractéristiques physiques. Ils inspirent beaucoup moins de dégoût dans la vieillesse. Ils ont le droit de vieillir, eux. Ils se bonifient même avec l'âge. 

> Occuper une position dominante dans l’économie, la politique, les relations amoureuses et familiales, mais aussi dans la création artistique et littéraire, leur permet d’être des sujets absolus et de faire des femmes des objets absolus.

Simone de Beauvoir parle de l'Autre. La femme est la figure d'altérité de l'homme, un être relatif, relatif à l'absolu de l'homme, la négation. Elle consacre toute une partie aux Mythes, donc aux représentations qu'on pouvait faire de la femme. Je pose là le [résumé en florilège glorieux](https://fr.wikipedia.org/wiki/Le_Deuxi%C3%A8me_Sexe#Chapitre_II) de pensées de 5 écrivains à 5 époques différentes (spoiler : à part Stendhal, c'est assez médiocre il semblerait).  

Pour illustration, son analyse sur Claudel :  

> En un sens, il semblerait que la femme ne saurait être exaltée davantage. Mais au fond, Claudel ne fait qu'exprimer poétiquement la tradition catholique légèrement modernisée [...]. Vénérant la femme en Dieu on la traitera en ce monde comme une servante : et même plus on exigera d'elle une soumission entière, plus sûrement on l'acheminera  sur la voie de son salut. [...] L'homme donne son activité, la femme sa personne ; sanctifier cette hiérarchie au nom de la volonté divine, ce n'est en rien modifier, mais au contraire prétendre la figer dans son éternel.
>
> **Simone de Beauvoir**, *Le Deuxième Sexe*

<a name="interiorisation"></a>

### Et de l'intériorisation

Alors oui, je crois qu'on a fini par intérioriser une bonne partie des représentations véhiculées par ce Système.  
On a intériorisé la place de la femme dans notre société avec ses compétences et ses métiers genrés et qui sont pour elle plus ou moins légitimes. On a intériorisé la bonne conduite sociale (ne sois pas trop bruyante). On a intériorisé la bonne conduite au sein du couple (sois serviable et ménagère). On a compris qu'il ne fallait pas trop ouvrir notre gueule, en dépit de ce qu'on ressent, face à des experts du corps de l'homme. Et on a pris l'habitude de ne pas être écoutées ni prises au sérieux. Alors oui, on reproduit les mêmes schémas, on se base sur les mêmes modèles que ceux qu'on a connus, on inculque l'oppression à nos sœurs, on accepte la soumission.  

Et quand on essaye d'en sortir, ou de se poser des questions, les réactions sont violentes. Rien que le fait de ne pas retirer ses poils de jambes ou d'aisselles déclenche des réactions du type "Ah c'est dégueulasse !" ou "C'est une insulte à la féminité !". De la part d'hommes ET de femmes. Alors à quel genre de réaction doit-on s'attendre face à des problèmes plus importants que celui du poil ?

Un exemple parmi _tant d'autres_ (cité dans *Sorcières*) est l'article [Why you are not married](https://www.huffpost.com/entry/why-youre-not-married_b_822088) ⚭, qui donne les raisons pour lesquelles une femme ne serait pas encore mariée, alors qu'elle le voudrait (vraiment ?) : "The problem is not men, it's you". La femme qui se poserait cette question passé la trentaine (quelle vieille peau), serait forcément soit trop en colère, superficielle, salope, menteuse et/ou égoïste. Au lieu de se pencher sur les tenants structurels des dysfonctionnements possibles au sein d'un couple hétérosexuel monogame, on réduit donc la tambouille et on la fait peser sur l'individu... de sexe féminin.

D'ailleurs, pour mettre en place des mesures répressives, anti-féministes, il n'y a rien de mieux que de laisser des femmes s'en charger, [comme le fait la teigne pâle qu'est le président russe](https://www.theguardian.com/commentisfree/2017/mar/22/vladimir-allies-russias-iron-ladies-useful-anti-feminists).

<a name="emancipation"></a>
## Des volontés d'émancipation

Je ne vais pas faire une histoire des luttes féministes, ni [du féminisme](https://fr.wikipedia.org/wiki/F%C3%A9minisme). Des initiatives pour une moindre oppression de la femme et pour plus d'égalité entre femmes et hommes, il y en a eu beaucoup au cours de l'Histoire. Mais toutes, qu'elles aient été revendicatives ou autonomes ont été matées (les béguines par exemple, pour leur "double refus d'obéissance, au prêtre et à l'époux").  
Nombre de personnes ont plaidé contre l'essentialisation de la condition de la femme, qui n'est due (de plus en plus d'études le montrent) qu'à une éducation et une socialisation genrées.  

D'ailleurs, Stendhal, cité par Simone de Beauvoir écrivait déjà :

> "Des pédants nous répètent depuis deux mille ans que les femmes ont l'esprit plus vif et les hommes plus de solidité ; que les femmes ont plus de délicatesse dans les idées et les hommes plus de force d'attention. Un badaud de Paris qui se promenait autrefois dans les jardins de Versailles concluait aussi de tout ce qu'il voyait que les arbres naissent taillés"
>
> **Stendhal**, *De l'amour*

(je disais bien qu'il s'en sortait pas si mal..)

* Et un *Podcast à soi* : [Les femmes sont-elles des hommes comme les autres?](https://www.arteradio.com/son/61660501/les_femmes_sont_elles_des_hommes_comme_les_autres_12)

Le *statu quo* actuel met en lumière l'importance de continuer la lutte ✊. Et certains événements récents montrent des manières inédites et précieuses de mener la lutte.

<a name="parole"></a>

### L'importance de donner la parole

Le combat féministe passe aussi par la parole. Il n'est pas uniquement question de laisser les femmes parler. Il est question d'une prise en compte de ce qu'elles vivent et ressentent. Il est question d'une création d'espaces d'expression -- publics, sociaux, scientifiques, littéraires, cinématographiques, politiques, médiatiques... pas que des posts de blog ! --  afin que leur, notre parole soit *écoutée*, *entendue* et *comprise*.

La prise de parole, qui est difficile, peut être libératrice. Ça a été illustré par le mouvement *#MeToo/#BalanceTonPorc*, [qui n'a pas forcément été bien accueilli par le public masculin](https://www.slate.fr/story/152585/balance-ton-porc-chasse-lhomme-ou-chasse-aux-femmes).

Et en fait, quand on sacre un enfoiré comme Polanski à peine deux ans après un mouvement censé libérer la parole de femmes victimes de violences sexuelles et sexistes, on se dit qu'on se fout un peu de notre poire. 

Polanski, ce n'est pas qu'on veut le censurer, lui ou son oeuvre, je pense. C'est qu'on veut qu'on se pose la question du pourquoi. Pourquoi est-ce qu'un octogénère accusé de viol qui a une carrière accomplie, est glorifié lors d'une cérémonie des Césars ? Pourquoi c'est un type qui a fui la justice qui a tous les financements qu'il veut ? Pourquoi est-ce que c'est à lui qu'on donne la parole, lorsqu'il raconte à travers l'histoire de Dreyfus sa propre histoire de soi-disant persécution et lynchages ? Pourquoi on ne donnerait pas plus la parole à du sang neuf ? Plus de thunes aux jeunes talents ?

Parce qu'en donnant la parole, en donnant du crédit, en donnant de l'argent on donne du pouvoir. Et ce pouvoir protège, légitime non seulement l'artiste, mais aussi l'homme. 🤴

* Là-dessus : [Eric Fassin, Séparer l'artiste de l'œuvre](https://blogs.mediapart.fr/eric-fassin/blog/020320/polanski-lhomme-et-lartiste) et tellement d'autres...

*Et en attendant que les choses changent, on se tape chaque année [l'éternelle misogynie de Woody Allen](https://www.monde-diplomatique.fr/2000/05/BRASSART/2292) sur les écrans.*

<a name="mouvsoc"></a>

### Un mouvement social comme les autres ?

Les parallèles avec d'autres luttes sociales sont criants.

On l'ignore tant que cela ne nous concerne pas. Comme les violences policières qui se sont déplacées des banlieues, vers les gilets jaunes jusqu'aux manifestant·e·s contre la réforme des retraites (2019/2020). Maintenant taguer ACAB partout est devenu mainstream.

Le féminisme se réapproprie les symboles qui avaient été utilisés pour opprimer, comme [celui de la sorcière](https://www.monde-diplomatique.fr/2018/10/CHOLLET/59161) qu'on transforme en étendard d'un féminisme contemporain. 

Comme au sein de tous les mouvements, des points de vue divergent, entre féminisme libéral (une énième récupération du capitalisme), radical (anti-patriarcal), matérialiste (anti-patriarcal, par opposition à l'essentialisme : "On ne naît pas femme, on le devient" *Simone de Beauvoir*)... Mais plus récemment deux grand courants s'opposent: le courant intersectionnel (antiraciste, anticapitaliste) et l'universaliste (toutes les femmes, quel que soit leur statut social, subiraient universellement une discrimination). 

%%%%%%%

Concernant ces deux derniers, mon avis est que les universalistes ne sont que des : [Arrêt sur images, le "silence" des intersecionnelles face à la prise de l'Afghanistan par les talibans](https://www.arretsurimages.net/articles/femmes-afghanes-haro-sur-le-silence-feministe)   

%%%%%%%

Un exemple récent était le cortège non-mixte (plutôt en mixité choisie) du 7 mars 2020 -- [dont voici l'appel](https://paris.demosphere.net/rv/79396) ainsi que la [vidéo](https://www.facebook.com/AFAPB/videos/809633426214468). Événement créé parce que [nombreuses femmes ne se retrouvaient pas dans le féminisme de #NousToutes, jugé trop exclusif](https://www.streetpress.com/sujet/1583768995-8-mars-rassemblement-feministes-intersectionnelles-radicales-manifestation-droit-femmes-journee). Il y a déjà 4 ans [la situation était grosso merdo la même](https://www.streetpress.com/sujet/1488962026-feminisme-blanc-bourgeois).  
Malgré les désaccords, c'est la diversité des pensées qui composent le mouvement qui crée sa richesse... j'imagine.

Mais comme de nombreuses luttes, celle-ci est d'encore plus longue haleine et fait face à une opposition d'un anti-féminisme virulent à celui juste chiant et ennuyeux qui [rabâche les mêmes arguments over and over again](https://www.terrafemina.com/article/feminisme-petite-liste-des-arguments-contre-le-feminisme-et-comment-y-repondre_a348392/1). Donc même si de nombreuses choses ont été accomplies, on n'en verra sans doute pas l'aboutissement de si tôt.  
Est-ce qu’on sait même comment se défaire de tout un système de domination ?  

<a name="antifem"></a>
### L'antiféminisme malgré tout

D'autant qu'un courant a l'encontre de la volonté d'émancipation des femmes et à l'encontre d'une volonté d'égalité entre femmes et hommes s'est également installé dans le paysage.  
Il est représenté par un rejet du féminisme et de ses luttes, allant d'une opposition frontale, en passant par des affirmations du genre "ça y est, l'égalité est atteinte, faut arrêter de s'exciter, là" allant même jusqu'à dire qu'on se serait retrouvé·e·s dans une société dominée par les femmes et se traduit par [une crise de la masculinité](https://www.erudit.org/fr/revues/rf/2012-v25-n1-rf0153/1011118ar/). (gros LOL)  

Une grosse tendance est celle de la décrédibilisation de mouvement, par exemple en disant qu'il ne s'agit que de femmes qui veulent mener une guerre contre les hommes. Mais elle est où cette guerre ? Genre.. comment ?  
La guerre des hommes contre les femmes -- oui. Elle est millénaire et réelle. Viols, insultes, harcèlements, violences, violences sexuelles (1/3 des femmes au cours de leur vie aujourd'hui), agressions, meurtres (au moins 151 féminicides en France en 2019) et j'en passe. Parce que ce sont des femmes.  
Des meurtres de masse ont été perpétrés spécifiquement contre des femmes, dans l'histoire récente : la tuerie de l'école polytechnique de Montréal en 1989. L'auteur, dans sa lettre d'adieu, avait revendiqué cet attentat comme antiféministe parce "les féministes avaient ruiné [sa] vie"

Un exemple, la quintessence-même de l'antiféminisme dans les années 70 avec *La lettre ouverte aux bonnes femmes* de Jean Lartéguy. On lit dans le résumé : 

> Les « bonnes femmes », celles qui prétendent libérer leur sexe de l’oppression masculine, les SGUM, les Women’s Libs, aux États-Unis, les M.L.F. en France, auxquelles viennent se joindre les « Lesbiennes Radicales », le Front Homosexuel d’Action Révolutionnaire, les W.I.T.C.H. « conspiration terroriste féminine internationale de l’enfer », etc., ne cherchent en vérité qu’à remplacer notre vieux système patriarcal, infiniment tolérant, qui ne survit que par l’habitude, par un matriarcat tout puissant et esclavagiste. Leur rêve, qu’elles déguisent dans une phraséologie pseudo-révolutionnaire : Asservir l’homme. Dans quel but ? Aucun ! Elles se disent le Tiers Monde opprimé alors qu’elles oppriment et détiennent la plus grande partie de la fortune du monde. Elles se disent esclaves : elles ne le sont que de la mode et de toutes les modes, intellectuelles ou autres qu’elles s’inventent. C’est cette gigantesque escroquerie — celle de la femme esclave — que vient dénoncer Lartéguy. En même temps qu’il pousse le premier cri de révolte de l’homme opprimé par l’impérialisme femelle.  

Misogynie criante, incompréhension, refus et peur de l'égalité mélangé aux mythes de la sorcière et de la femme castratrice. C'est tout.

<a name="revolte"></a>
### Et de la révolte ?

Dans *Sorcières*, une œuvre est régulièrement prise comme référence sur la question de la chasse aux sorcières, il s'agit de *La Sorcière et l’Occident* de Guy Bechtel. Après une histoire bien documentée des horreurs et des massacres des "sorcières", il arrive à la conclusion assez cynique suivante :  

> Il estime que cet épisode s’inscrivait dans le cadre d’une « révolution », et **les révolutions, argue-t-il, « ne peuvent s’accomplir que par l’anéantissement des positions adverses et de ceux qui les soutiennent (ou dont on prétend qu’ils les soutiennent) »**. Il affirme : « Le mouvement qui a voulu tuer les sorcières, inconsciemment bien sûr, est aussi celui qui plus tard a fait naître et penser Montesquieu, Voltaire et Kant. » En somme, il donne sa bénédiction à la logique qu’il a lui-même résumée par cette formule : « Tuer les femmes anciennes pour fabriquer l’homme nouveau ». (...)  
> Un point de vue à comparer avec celui, très différent, de Barbara Ehrenreich et Deirdre English : elles évoquent non seulement les tragédies individuelles – les aspirations étouffées et l’élan brisé des victimes –, mais aussi tout ce dont la société s’est privée en les pourchassant, tout ce qu’elles ont été empêchées de développer et de transmettre. Elles parlent d’un « immense gaspillage de talent et de connaissance » et invitent « à regagner, ou au moins à pointer du doigt, ce qui a été perdu » 
> *Sorcières*

(j'ai été un peu sur le cul en lisant ça..)

L'homme nouveau qui n'a su se frayer un chemin dans l'existence qu'en soumettant ses semblables et construire une Histoire à un seul sexe, je l'exècre, en fait. 
Le passage en gras, je l'utiliserais d'ailleurs volontiers pour le féminisme aujourd'hui : il faut une révolution, avec ce que ça implique d'anéantissement des positions adverses représentées par le patriarcat et de ceux qui le soutiennent. Mais ici il faudrait le voir de façon figurative : extrêmement rares sont celles qui songeraient à mener une chasse aux sorcières inversée.  
Et le féminisme radical ce n'est "que" ça : la volonté de détruire le patriarcat, pas les hommes. Et c'est ce que signifie le slogan, en miroir d'ACAB, « Men are trash ».

Oui, des femmes veulent se rebeller, anéantir une vision de la féminité qui les objectifie et les installe dans un carcan qui les aliène. À l'instar des FEMEN : elles essayent d'inverser les codes de sexualisation de leurs corps en les érigeant en étendards politiques. 🪧  
Certaines en ont raz-le-bol et estiment que maintenant la seule possibilité qui reste c'est de [se lever et se barrer](https://www.liberation.fr/debats/2020/03/01/cesars-desormais-on-se-leve-et-on-se-barre_1780212).  
Certaines pensent et vivent également un monde, du moins des communautés, sans hommes. C'est un peu les idées de Virginie Despentes, et elle en parle je crois [ici](https://www.binge.audio/virginie-despentes-meuf-king-kong/). Mais est-ce exagéré de vouloir se soustraire (radicalement) à son oppresseur au vu de tout ce que certaines ont pu subir ? 

<a name="content"></a>
### Et pourquoi on peut s'en réjouir

Je devrais avoir des scrupules à terminer un texte avec une citation de l'*Alpha Molester*, le *Pig Donald*, mais j'assume. Car pour mesurer l'étendue de l'ignorance sur un sujet, il n'y a pas mieux. Car il est de bonne pratique de prendre ce qu'il dit et de faire ou penser exactement le contraire :  

> I wouldn’t say I'm a feminist. 
> I mean, I think that would be, maybe, going too far. 
> I’m for women, I’m for men, I’m for everyone.”

En miroir de la réponse réac' au mouvement #BlackLivesMatter, #AllLivesMatter (sans parler de #WhiteLivesMatter la version extrême-droite identitaire).

L'exemple de #MeToo prouve que ce genre de poussées peuvent être libératrices pas uniquement pour les victimes, mais pour la société toute entière : [7 positive changes that have come from the #MeToo movement](https://www.vox.com/identities/2019/10/4/20852639/me-too-movement-sexual-harassment-law-2019). 

On peut continuer avec une [énumération de ce que les hommes gagneraient avec le féminisme](https://www.bloomingyou.fr/gagnent-hommes-feminisme/). En somme, le féminisme pourrait être libérateur et émancipateur autant pour la femme que pour l'homme et permettrait carrément de rendre la planète plus verte ! (Wow !)  

Mais pour plus de subtilité, il y a Victoire Tuaillon dans *Les couilles sur la table* qui fait un énorme travail de décryptage de la société patriarcale et met en valeur ce que chacun·e aurait à gagner en en sortant. [Notamment dans l'amour](https://www.binge.audio/ce-que-le-patriarcat-fait-a-lamour-in-english/). 


<div class="img-with-text">
<img src="/images/vasilisa.jpg" alt="Vasilisa Prekrasnaya" style="float:center"  width="450"
	title="Vassilissa"
	/>
    <p><font size="2"><a href="https://fr.wikipedia.org/wiki/Vassilissa-la-tr%C3%A8s-belle">Vassilissa Prekrasnaya</a> portant le crâne aux yeux ardents<br> qui consumera ses oppresseuses. <i>Illustration <a href="http://www.fairyroom.ru/?p=54619">Alexander Koshkin</a></i></font></p>
</div>
---

C'était un survol très grossier et partial d'un historique et du contexte actuel du féminisme, qui tentait au final de répondre aux questions "pourquoi les meufs sont vénères ? Et vont-elles manger les hommes ?", mais aussi de montrer qu'elles en ont bien le droit.

J'ai, à coup sûr, oublié des éléments, et ne suis rentrée que peu dans les détails de certains thèmes.

Je n'ai pas parlé de l'humour sexiste (il faut pas en faire). 

J'ai peu parlé de l'éducation : du non-accès des filles à l'éducation et surtout l'éducation supérieure avant ; de l'éducation très genrée pendant la petite enfance aujourd'hui (il suffit de lire ou regarder quelques contes pour enfants) qui perpétue tous les stéréotypes sexistes et imprègne les gamins. C'est bien sûr cette dernière qu'il faut revoir pour pouvoir espérer des changements radicaux et durables dans nos esprits.

Je n'ai pas non-plus écrit sur les "femmes fortes" d'aujourd'hui, [étendards du capitalisme moderne](https://www.deuxiemepage.fr/2019/09/19/feminisme-pour-les-99-manifeste-chronique/), qui se fraient un chemin dans la hiérarchie du monde de l'entreprise, qui souvent doivent, pour y parvenir, être hyperqualifiées en comparaison à leurs collègues mecs, mais qui parfois aussi exacerbent des caractéristiques qu'on présente comme masculines (carriérisme, compétition, dureté...) afin de pouvoir se créer une place dans un monde très masculin.

Je n'ai pas parlé de la prostitution.  

Je n'ai pas parlé du traitement réservée aux femmes en psychiatrie. 

 Je n'ai pas...

---

<a name="refs"></a>
### [ Références supplémentaires en vrac ](/refs-meufs-veneres/)

